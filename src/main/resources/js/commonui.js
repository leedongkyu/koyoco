

$(function(){
/****************************** Brower Check S ******************************/
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;
    var device;
    if(!isMobile) {
          device = 'pcweb'
     } else {
          device = 'mobile'
     }

    var agent = navigator.userAgent.toLowerCase();
    var browser;
    if(agent.indexOf('msie') > -1){
        browser = 'ie' + agent.match(/msie (\d+)/)[1]
    } else if(agent.indexOf('trident') > -1){
        browser = 'ie11'
    } else if(agent.indexOf('edge') > -1){
        browser = 'edge'
    } else if(agent.indexOf('firefox') > -1){
        browser = 'firefox'
    } else if(agent.indexOf('opr') > -1){
        browser = 'opera'
    } else if(agent.indexOf('chrome') > -1){
        browser = 'chrome'
    } else if(agent.indexOf('safari') > -1){
        browser = 'safari'
    }

    var bodyClass = browser + ' ' + device;
    $('body').addClass(bodyClass);
/****************************** Brower Check S ******************************/



/****************************** URL Check S ******************************/
    // var url = document.location.href,
    //     htmlUrl = url.substring(url.indexOf('?') + 1).split('&'),
    //     htmlCate = htmlUrl[0].split('='),
    //     htmlCateId = htmlCate[1],
    //     htmlSub = htmlUrl[1].split('='),
    //     htmlSubId = htmlSub[1],
    //     htmlPage = htmlUrl[2].split('='),
    //     htmlPageId = htmlPage[1];
/****************************** URL Check E ******************************/



/****************************** UI Layout S ******************************/
    // Name
/****************************** UI Layout E ******************************/



/****************************** DatePicker S ******************************/
    // Name
    function datePickerTui(){
        var uiDatepicker = '<div class="tui-datepicker-input">';
            uiDatepicker += '<input type="text" title="날짜선택">';
            uiDatepicker += '<span class="tui-ico-date"></span>';
        uiDatepicker += '</div>';
        uiDatepicker += '<div class="date-picker-wrapper"></div>';

        $('.date-picker').append(uiDatepicker);
        $('.date-picker').each(function(i){
            if($(this).parents('*').hasClass('unit')){
                if($(this).index() == 0){
                    $(this).closest('.unit').attr('id', 'date-picker-' + i);
                    $(this).addClass('start');
                } else if (! $(this).index() == 0){
                    $(this).addClass('end');
                    var target = $(this).closest('.unit').attr('id');
                    var today = new Date();
                    var picker = tui.DatePicker.createRangePicker({
                        startpicker: {
                            date: today,
                            input: '#' + target +' .start input',
                            container: '#' + target +' .date-picker.start .date-picker-wrapper'
                        },
                        endpicker: {
                            date: today,
                            input: '#' + target +' .end input',
                            container: '#' + target +' .date-picker.end .date-picker-wrapper'
                        },
                        selectableRanges: [
                            [today, new Date(today.getFullYear() + 1, today.getMonth(), today.getDate())]
                        ]
                    });
                }
            } else {
                $(this).attr('id', 'date-picker-' + i);
                var datepicker = new tui.DatePicker('#date-picker-' + i +' .date-picker-wrapper', {
                    date: new Date(),
                    input: {
                        element: '#date-picker-' + i +' .tui-datepicker-input input',
                        format: 'yyyy-MM-dd'
                    }
                });
            }
        });
    }
    if($('.date-picker').length > 0){
        datePickerTui();
    }
/****************************** DatePicker E ******************************/
});



/****************************** Chart S ******************************/
    // Name
/****************************** Chart E ******************************/



/****************************** Toggle S ******************************/
    // Base
    $(document).on('click', '.toggle-box .btn-allmenu', function(){
        $(this).parents('.toggle-box').find('*[aria-expanded]').attr('aria-expanded','true');
    });
    $(document).on('click', '*[aria-expanded]', function(){
        if($(this).attr('aria-expanded') == 'true'){
            $(this).attr('aria-expanded','false');
        } else {
            $(this).attr('aria-expanded','true');
        }
    });
/****************************** Toggle E ******************************/



/****************************** Tab S ******************************/
    // Base
    $(document).on('click', '*[role="tablist"] button', function(){
        var target = $(this).attr('aria-controls');
        $(this).attr('aria-selected','true').siblings('button').attr('aria-selected','false');
        $('#' + target).addClass('active').siblings('*[role="tabpanel"]').removeClass('active');
    });
/****************************** Toggle E ******************************/



/****************************** Alert & Confirm S ******************************/
    /* Call EX
        $(document).on('click', '.open2', function(){
            alertOpen(
                $(this), // Accessibility
                'tit', // PopupTitle
                'txt', // PopupText
                'btnNo', // btnNoName
                'btnYes', // btnYesName
                alertClose, // btnNoCallback
                alertClose // btnYesCallback
            );
        });
    */
    // Base Function
    function alertOpen(tit,txt,btnNo,btnYes,btnNoCallback,btnYesCallback){
        var uiLayoutAlert = '<div class="cmm-alert active" tabindex="0" aria-hidden="false">';
            uiLayoutAlert += '<div class="content">';
                tit == '' ? '' : uiLayoutAlert += '<div class="tit">'+ tit +'</div>';
                txt == '' ? '' : uiLayoutAlert += '<div class="txt">'+ txt +'</div>';
                uiLayoutAlert += '<div class="btn-box">';
                    btnNo == '' ? '' : uiLayoutAlert += '<button type="button" class="btn-no"><span>'+ btnNo +'</span></button>';
                    btnYes == '' ? '' : uiLayoutAlert += '<button type="button" class="btn-yes"><span>'+ btnYes +'</span></button>';
                uiLayoutAlert += '</div>';
            uiLayoutAlert += '</div>';
            uiLayoutAlert += '<div class="dim"></div>';
        uiLayoutAlert += '</div>';
        $('body').append(uiLayoutAlert);

        // Accessibility
        $('body').addClass('scroll-off').find('#view-wrap').attr('aria-hidden','true');
        $('.cmm-alert').focus();

        $(document).on('click', '.cmm-alert .btn-no', function(){
            btnNoCallback == '' ? '' : btnNoCallback();
            $('.cmm-alert').remove();
        });
        $(document).on('click', '.cmm-alert .btn-yes', function(){
            btnYesCallback == '' ? '' : btnYesCallback();
            $('.cmm-alert').remove();
        });
    }
/****************************** Alert & Confirm E ******************************/



/****************************** Layer Popup Open & Close S ******************************/
    // Open
    $(document).on('click', '*[data-layer-target]', function(){
        var target = $(this).attr('data-layer-target');
        $('body').addClass('scroll-off').find('#view-wrap').attr('aria-hidden','true').siblings('*').attr('aria-hidden','true');
        $('#' + target).addClass('active').attr('aria-hidden','false').focus();
    });
    // Close
    function layerClose(){
        var target = $('*[aria-hidden="false"].cmm-layer');
            targetID = target.attr('id');
        if($('*[data-layer-target="'+ targetID +'"]').parents('div').hasClass('.cmm-layer')){
            $('body').addClass('scroll-off').find('#view-wrap').attr('aria-hidden','true').siblings('.cmm-layer').attr('aria-hidden','true');
        } else {
            $('body').removeClass('scroll-off').find('#view-wrap').attr('aria-hidden','false').siblings('.cmm-layer').attr('aria-hidden','false');
        }
        target.removeClass('active').attr('aria-hidden','true');
        $('*[data-layer-target="'+ targetID +'"]').focus();
    }
    $(document).on('click', '.cmm-layer button.dim, .cmm-layer .close', function(){
        layerClose();
    });

    // Focus
    /*
    $(document).on('keydown', '.cmm-layer button.dim', function(e){
        if(e.keyCode == 9){
            $(this).closest('.cmm-layer').find('.header').find('.close').focus();
        }
    });
    $(document).on('keydown', '.cmm-layer .header .close', function(e){
        if(e.keyCode == 9 && e.shiftKey){
            $(this).parents('.cmm-layer').find('.dim').focus();
        }
    });
    */
/****************************** Layer Popup Open & Close E ******************************/


/****************************** Tree Check S ******************************/
    $(document).on('click', '.cmm-tree dt input', function(){
        var target = $(this).closest('dl').find('dd').find('input');
        if($(this).prop('checked') == true){
            target.prop('checked',true);
            target.closest('.cmm-checkbox').addClass('checked');
        }else{
            target.prop('checked',false);
            target.closest('.cmm-checkbox').removeClass('checked');
        }
    });
    $(document).on('click', '.cmm-tree dt > button', function(){
        if($(this).closest('dl').hasClass('active')){
            $(this).closest('dl').removeClass('active');
        }else{
            $(this).closest('dl').addClass('active');
        }
    });
    /*
    $(document).on('change', '.cmm-tree dd input', function(){
        $(this).parents('dl').find('dd').find('input').each(function(i){
            if($(this).prop('checked') == false){
                checkState = false;
                return false;
            }
            checkState = true;
        });

        var target = $(this).parents('dl').find('dt').find('input');
        if(checkState){
            target.prop('checked',true);
            target.parents('.cmm-checkbox').addClass('checked');
        }else{
            target.prop('checked',false);
            target.parents('.cmm-checkbox').removeClass('checked');
        }
    });
    */
/****************************** Tree Check E ******************************/



/****************************** Sample S ******************************/
    // Name
/****************************** Sample E ******************************/