package com.koyoco.his.Service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.koyoco.his.Vo.CountSearch;

@Service
public interface TestService {
     public CountSearch selectId(Map<String, Object> map);
     public List<CountSearch> selectIds(Map<String, Object> map);
}