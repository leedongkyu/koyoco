package com.koyoco.his.Service;

import com.koyoco.his.Vo.Save.TbMemberMUserVO;
import com.koyoco.his.Vo.find.MemberDetailUserVO;
import com.koyoco.his.Vo.find.MemberTransformVO;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public interface MemberService {

    public String updateInfo(TbMemberMUserVO tbMemberMUserVO);

    public MemberDetailUserVO findUser(Map<String,Object> map);

    public MemberTransformVO findTrfUser(Map<String,Object> map);

    public String transInsert(MemberTransformVO map);

}
