package com.koyoco.his.Service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.koyoco.his.Vo.find.MemberSearchUserVO;

@Service
public interface SignUpService {
	
	public String signUpSave(Map<String,Object> param);
	
	public List<MemberSearchUserVO> loginCheck(Map<String,Object> map);
}
