package com.koyoco.his.Service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.koyoco.his.Vo.find.BoardThesisDetailVO;
import com.koyoco.his.Vo.find.BoardVO;

@Service
public interface BoardService {
	public List<BoardVO> search(Map<String,Object> param);
	
	public BoardThesisDetailVO detailSearch(Map<String, Object> param);
	
	public String registerSave(Map<String,Object> param);
}
