package com.koyoco.his.Vo.Save;

import java.sql.Date;

public class TbBoardMAcademicFileVO {

	// 학술/세미나 게시글 파일
	private Integer fileSeq;
	private Integer acmSeq;
	private String fileOriginNm;
	private String fileSaveNm;
	private String filePath;
	private String fileExtns;
	private String regUser;
	private Date regDate;
	private String updUser;
	private Date updDate;

	public Integer getFileSeq() {
		return fileSeq;
	}

	public void setFileSeq(Integer fileSeq) {
		this.fileSeq = fileSeq;
	}

	public Integer getAcmSeq() {
		return acmSeq;
	}

	public void setAcmSeq(Integer acmSeq) {
		this.acmSeq = acmSeq;
	}

	public String getFileOriginNm() {
		return fileOriginNm;
	}

	public void setFileOriginNm(String fileOriginNm) {
		this.fileOriginNm = fileOriginNm;
	}

	public String getFileSaveNm() {
		return fileSaveNm;
	}

	public void setFileSaveNm(String fileSaveNm) {
		this.fileSaveNm = fileSaveNm;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileExtns() {
		return fileExtns;
	}

	public void setFileExtns(String fileExtns) {
		this.fileExtns = fileExtns;
	}

	public String getRegUser() {
		return regUser;
	}

	public void setRegUser(String regUser) {
		this.regUser = regUser;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public String getUpdUser() {
		return updUser;
	}

	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	public Date getUpdDate() {
		return updDate;
	}

	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
}
