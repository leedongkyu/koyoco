package com.koyoco.his.Vo.Save;

import java.sql.Date;

public class TbMemberSBusinessVO {
	
	// 기업 유저 정보
	private String userId;
	private String gblId;
	private String buisNm;
	private String repNm;
	private String repItem;
	private String buisCmt;
	private String buisNotice;
	private String noticeCd;
	private String buisUrl;
	private String buisAdd;
	private Integer buisZipcd;
	private Integer buisTel;
	private String buisPhone;
	private String buisSeqNum;
	private String regUser;
	private Date regDate;
	private String updUser;
	private Date updDate;


	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getGblId() {
		return gblId;
	}

	public void setGblId(String gblId) {
		this.gblId = gblId;
	}

	public String getBuisNm() {
		return buisNm;
	}

	public void setBuisNm(String buisNm) {
		this.buisNm = buisNm;
	}

	public String getRepNm() {
		return repNm;
	}

	public void setRepNm(String repNm) {
		this.repNm = repNm;
	}

	public String getRepItem() {
		return repItem;
	}

	public void setRepItem(String repItem) {
		this.repItem = repItem;
	}

	public String getBuisCmt() {
		return buisCmt;
	}

	public void setBuisCmt(String buisCmt) {
		this.buisCmt = buisCmt;
	}

	public String getBuisNotice() {
		return buisNotice;
	}

	public void setBuisNotice(String buisNotice) {
		this.buisNotice = buisNotice;
	}

	public String getNoticeCd() {
		return noticeCd;
	}

	public void setNoticeCd(String noticeCd) {
		this.noticeCd = noticeCd;
	}

	public String getBuisUrl() {
		return buisUrl;
	}

	public void setBuisUrl(String buisUrl) {
		this.buisUrl = buisUrl;
	}

	public String getBuisAdd() {
		return buisAdd;
	}

	public void setBuisAdd(String buisAdd) {
		this.buisAdd = buisAdd;
	}

	public Integer getBuisZipcd() {
		return buisZipcd;
	}

	public void setBuisZipcd(Integer buisZipcd) {
		this.buisZipcd = buisZipcd;
	}

	public Integer getBuisTel() {
		return buisTel;
	}

	public void setBuisTel(Integer buisTel) {
		this.buisTel = buisTel;
	}

	public String getBuisPhone() {
		return buisPhone;
	}

	public void setBuisPhone(String buisPhone) {
		this.buisPhone = buisPhone;
	}

	public String getBuisSeqNum() {
		return buisSeqNum;
	}

	public void setBuisSeqNum(String buisSeqNum) {
		this.buisSeqNum = buisSeqNum;
	}

	public String getRegUser() {
		return regUser;
	}

	public void setRegUser(String regUser) {
		this.regUser = regUser;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public String getUpdUser() {
		return updUser;
	}

	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	public Date getUpdDate() {
		return updDate;
	}

	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
}
