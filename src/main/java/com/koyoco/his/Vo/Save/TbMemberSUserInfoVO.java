package com.koyoco.his.Vo.Save;

import java.sql.Date;

public class TbMemberSUserInfoVO {

	// 유저 정보 상세
	private String userId;
	private String gblId;
	private String userAccess;
	private String userAdd;
	private Integer userZipcd;
	private String billBank;
	private Integer tellNum;
	private String billAccountn1;
	private String billAccountn2;
	private String billAccountn3;
	private String billAccountn4;
	private String regUser;
	private Date regDate;
	private String updUser;
	private Date updDate;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getGblId() {
		return gblId;
	}

	public void setGblId(String gblId) {
		this.gblId = gblId;
	}

	public String getUserAccess() {
		return userAccess;
	}

	public void setUserAccess(String userAccess) {
		this.userAccess = userAccess;
	}

	public String getUserAdd() {
		return userAdd;
	}

	public void setUserAdd(String userAdd) {
		this.userAdd = userAdd;
	}

	public Integer getUserZipcd() {
		return userZipcd;
	}

	public void setUserZipcd(Integer userZipcd) {
		this.userZipcd = userZipcd;
	}

	public String getBillBank() {
		return billBank;
	}

	public void setBillBank(String billBank) {
		this.billBank = billBank;
	}

	public Integer getTellNum() {
		return tellNum;
	}

	public void setTellNum(Integer tellNum) {
		this.tellNum = tellNum;
	}

	public String getBillAccountn1() {
		return billAccountn1;
	}

	public void setBillAccountn1(String billAccountn1) {
		this.billAccountn1 = billAccountn1;
	}

	public String getBillAccountn2() {
		return billAccountn2;
	}

	public void setBillAccountn2(String billAccountn2) {
		this.billAccountn2 = billAccountn2;
	}

	public String getBillAccountn3() {
		return billAccountn3;
	}

	public void setBillAccountn3(String billAccountn3) {
		this.billAccountn3 = billAccountn3;
	}

	public String getBillAccountn4() {
		return billAccountn4;
	}

	public void setBillAccountn4(String billAccountn4) {
		this.billAccountn4 = billAccountn4;
	}

	public String getRegUser() {
		return regUser;
	}

	public void setRegUser(String regUser) {
		this.regUser = regUser;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public String getUpdUser() {
		return updUser;
	}

	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	public Date getUpdDate() {
		return updDate;
	}

	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
}
