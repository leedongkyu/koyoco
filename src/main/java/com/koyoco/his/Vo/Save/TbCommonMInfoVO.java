package com.koyoco.his.Vo.Save;

import java.sql.Date;

public class TbCommonMInfoVO {

	// 공통 테이블 마스터
	private String cdCd;
	private String cdNm;
	private String cdYn;
	private String regUser;
	private Date regDate;
	private String updUser;
	private Date updDate;

	public String getCdCd() {
		return cdCd;
	}

	public void setCdCd(String cdCd) {
		this.cdCd = cdCd;
	}

	public String getCdNm() {
		return cdNm;
	}

	public void setCdNm(String cdNm) {
		this.cdNm = cdNm;
	}

	public String getCdYn() {
		return cdYn;
	}

	public void setCdYn(String cdYn) {
		this.cdYn = cdYn;
	}

	public String getRegUser() {
		return regUser;
	}

	public void setRegUser(String regUser) {
		this.regUser = regUser;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public String getUpdUser() {
		return updUser;
	}

	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	public Date getUpdDate() {
		return updDate;
	}

	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
}
