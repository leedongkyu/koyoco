package com.koyoco.his.Vo.Save;

import java.sql.Date;

public class TbBoardSWishVO {

	// 논문 찜 목록 관리
	private Integer thesisSeq;
	private String userId;
	private Date regDate;

	public Integer getThesisSeq() {
		return thesisSeq;
	}

	public void setThesisSeq(Integer thesisSeq) {
		this.thesisSeq = thesisSeq;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}
}
