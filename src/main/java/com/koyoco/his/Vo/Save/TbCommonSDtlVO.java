package com.koyoco.his.Vo.Save;

import java.sql.Date;

public class TbCommonSDtlVO {

	// 공통 테이블 서브
	private String grpCd;
	private String cdCd;
	private String cdNm;
	private Integer cdSno;
	private String cdYn;
	private String cdDtl;
	private String regUser;
	private Date regDate;
	private String updUser;
	private Date updDate;

	public String getGrpCd() {
		return grpCd;
	}

	public void setGrpCd(String grpCd) {
		this.grpCd = grpCd;
	}

	public String getCdCd() {
		return cdCd;
	}

	public void setCdCd(String cdCd) {
		this.cdCd = cdCd;
	}

	public String getCdNm() {
		return cdNm;
	}

	public void setCdNm(String cdNm) {
		this.cdNm = cdNm;
	}

	public Integer getCdSno() {
		return cdSno;
	}

	public void setCdSno(Integer cdSno) {
		this.cdSno = cdSno;
	}

	public String getCdYn() {
		return cdYn;
	}

	public void setCdYn(String cdYn) {
		this.cdYn = cdYn;
	}

	public String getCdDtl() {
		return cdDtl;
	}

	public void setCdDtl(String cdDtl) {
		this.cdDtl = cdDtl;
	}

	public String getRegUser() {
		return regUser;
	}

	public void setRegUser(String regUser) {
		this.regUser = regUser;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public String getUpdUser() {
		return updUser;
	}

	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	public Date getUpdDate() {
		return updDate;
	}

	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
}
