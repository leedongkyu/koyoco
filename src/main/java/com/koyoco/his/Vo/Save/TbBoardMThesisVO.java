package com.koyoco.his.Vo.Save;

import java.sql.Date;

public class TbBoardMThesisVO {

	// 논문 게시글
	private Integer thesisSeq;
	private String userId;
	private String thesisTitle;
	private String langCd;
	private String thesisYear;
	private String thesisAuthor;
	private Integer thesisPrice;
	private Integer downCnt;
	private Integer viewCnt;
	private String authorIntro;
	private String docuIntro;
	private String addInfo;
	private String regUser;
	private Date regDate;
	private String updUser;
	private Date updDate;

	public Integer getThesisSeq() {
		return thesisSeq;
	}

	public void setThesisSeq(Integer thesisSeq) {
		this.thesisSeq = thesisSeq;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getThesisTitle() {
		return thesisTitle;
	}

	public void setThesisTitle(String thesisTitle) {
		this.thesisTitle = thesisTitle;
	}

	public String getLangCd() {
		return langCd;
	}

	public void setLangCd(String langCd) {
		this.langCd = langCd;
	}

	public String getThesisYear() {
		return thesisYear;
	}

	public void setThesisYear(String thesisYear) {
		this.thesisYear = thesisYear;
	}

	public String getThesisAuthor() {
		return thesisAuthor;
	}

	public void setThesisAuthor(String thesisAuthor) {
		this.thesisAuthor = thesisAuthor;
	}

	public Integer getThesisPrice() {
		return thesisPrice;
	}

	public void setThesisPrice(Integer thesisPrice) {
		this.thesisPrice = thesisPrice;
	}

	public Integer getDownCnt() {
		return downCnt;
	}

	public void setDownCnt(Integer downCnt) {
		this.downCnt = downCnt;
	}

	public Integer getViewCnt() {
		return viewCnt;
	}

	public void setViewCnt(Integer viewCnt) {
		this.viewCnt = viewCnt;
	}

	public String getAuthorIntro() {
		return authorIntro;
	}

	public void setAuthorIntro(String authorIntro) {
		this.authorIntro = authorIntro;
	}

	public String getDocuIntro() {
		return docuIntro;
	}

	public void setDocuIntro(String docuIntro) {
		this.docuIntro = docuIntro;
	}

	public String getAddInfo() {
		return addInfo;
	}

	public void setAddInfo(String addInfo) {
		this.addInfo = addInfo;
	}

	public String getRegUser() {
		return regUser;
	}

	public void setRegUser(String regUser) {
		this.regUser = regUser;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public String getUpdUser() {
		return updUser;
	}

	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	public Date getUpdDate() {
		return updDate;
	}

	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
}
