package com.koyoco.his.Vo.Save;

import java.sql.Date;

public class TbBoardMThesisFileVO {
	
	// 논문 게시글 파일
	private Integer fileSeq;
	private Integer thesisSeq;
	private String fileOriginNm;
	private String fileSaveNm;
	private String filePath;
	private String fileExtns;
	private String regUser;
	private Date regDate;
	private String updUser;
	private Date updDate;

	public Integer getFileSeq() {
		return fileSeq;
	}

	public void setFileSeq(Integer fileSeq) {
		this.fileSeq = fileSeq;
	}

	public Integer getThesisSeq() {
		return thesisSeq;
	}

	public void setThesisSeq(Integer thesisSeq) {
		this.thesisSeq = thesisSeq;
	}

	public String getFileOriginNm() {
		return fileOriginNm;
	}

	public void setFileOriginNm(String fileOriginNm) {
		this.fileOriginNm = fileOriginNm;
	}

	public String getFileSaveNm() {
		return fileSaveNm;
	}

	public void setFileSaveNm(String fileSaveNm) {
		this.fileSaveNm = fileSaveNm;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileExtns() {
		return fileExtns;
	}

	public void setFileExtns(String fileExtns) {
		this.fileExtns = fileExtns;
	}

	public String getRegUser() {
		return regUser;
	}

	public void setRegUser(String regUser) {
		this.regUser = regUser;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public String getUpdUser() {
		return updUser;
	}

	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	public Date getUpdDate() {
		return updDate;
	}

	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
}
