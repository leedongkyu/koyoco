package com.koyoco.his.Vo.Save;

import java.sql.Date;

public class TbMemberMFileVO {
	
	// 회원 파일 마스터
	private Integer fileSeq;
	private Integer fileNo;
	private String userId;
	private String fileOriginNm;
	private String fileSaveNm;
	private String filePath;
	private String fileExtns;
	private String regUser;
	private Date regDate;
	private String updUser;
	private Date updDate;

	public Integer getFileSeq() {
		return fileSeq;
	}

	public void setFileSeq(Integer fileSeq) {
		this.fileSeq = fileSeq;
	}

	public Integer getFileNo() {
		return fileNo;
	}

	public void setFileNo(Integer fileNo) {
		this.fileNo = fileNo;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFileOriginNm() {
		return fileOriginNm;
	}

	public void setFileOriginNm(String fileOriginNm) {
		this.fileOriginNm = fileOriginNm;
	}

	public String getFileSaveNm() {
		return fileSaveNm;
	}

	public void setFileSaveNm(String fileSaveNm) {
		this.fileSaveNm = fileSaveNm;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileExtns() {
		return fileExtns;
	}

	public void setFileExtns(String fileExtns) {
		this.fileExtns = fileExtns;
	}

	public String getRegUser() {
		return regUser;
	}

	public void setRegUser(String regUser) {
		this.regUser = regUser;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public String getUpdUser() {
		return updUser;
	}

	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	public Date getUpdDate() {
		return updDate;
	}

	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
}
