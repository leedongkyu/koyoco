package com.koyoco.his.Vo.Save;

import java.sql.Date;

public class TbBoardMAcademicVO {

	// 학술/세미나 게시글
	private Integer acmSeq;
	private String userId;
	private String acmType;
	private String thesisTitle;
	private String docuIntro;
	private Date acmStartDt;
	private Date acmEndDt;
	private Integer viewCnt;
	private String regUser;
	private Date regDate;
	private String updUser;
	private Date updDate;

	public Integer getAcmSeq() {
		return acmSeq;
	}

	public void setAcmSeq(Integer acmSeq) {
		this.acmSeq = acmSeq;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAcmType() {
		return acmType;
	}

	public void setAcmType(String acmType) {
		this.acmType = acmType;
	}

	public String getThesisTitle() {
		return thesisTitle;
	}

	public void setThesisTitle(String thesisTitle) {
		this.thesisTitle = thesisTitle;
	}

	public String getDocuIntro() {
		return docuIntro;
	}

	public void setDocuIntro(String docuIntro) {
		this.docuIntro = docuIntro;
	}

	public Date getAcmStartDt() {
		return acmStartDt;
	}

	public void setAcmStartDt(Date acmStartDt) {
		this.acmStartDt = acmStartDt;
	}

	public Date getAcmEndDt() {
		return acmEndDt;
	}

	public void setAcmEndDt(Date acmEndDt) {
		this.acmEndDt = acmEndDt;
	}

	public Integer getViewCnt() {
		return viewCnt;
	}

	public void setViewCnt(Integer viewCnt) {
		this.viewCnt = viewCnt;
	}

	public String getRegUser() {
		return regUser;
	}

	public void setRegUser(String regUser) {
		this.regUser = regUser;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public String getUpdUser() {
		return updUser;
	}

	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	public Date getUpdDate() {
		return updDate;
	}

	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
}
