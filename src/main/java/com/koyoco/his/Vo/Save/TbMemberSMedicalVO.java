package com.koyoco.his.Vo.Save;

import java.sql.Date;

public class TbMemberSMedicalVO {

	// 병원 유저 정보
	private String userId;
	private String gblId;
	private String mdcNm;
	private String dotNm;
	private String dotNum;
	private String mdcNum;
	private String mdcNotice;
	private String noticeCd;
	private String mdcCd;
	private String mdcAdd;
	private Integer mdcZipcd;
	private Integer mdcTel;
	private String regUser;
	private Date regDate;
	private String updUser;
	private Date updDate;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getGblId() {
		return gblId;
	}

	public void setGblId(String gblId) {
		this.gblId = gblId;
	}

	public String getMdcNm() {
		return mdcNm;
	}

	public void setMdcNm(String mdcNm) {
		this.mdcNm = mdcNm;
	}

	public String getDotNm() {
		return dotNm;
	}

	public void setDotNm(String dotNm) {
		this.dotNm = dotNm;
	}

	public String getDotNum() {
		return dotNum;
	}

	public void setDotNum(String dotNum) {
		this.dotNum = dotNum;
	}

	public String getMdcNum() {
		return mdcNum;
	}

	public void setMdcNum(String mdcNum) {
		this.mdcNum = mdcNum;
	}

	public String getMdcNotice() {
		return mdcNotice;
	}

	public void setMdcNotice(String mdcNotice) {
		this.mdcNotice = mdcNotice;
	}

	public String getNoticeCd() {
		return noticeCd;
	}

	public void setNoticeCd(String noticeCd) {
		this.noticeCd = noticeCd;
	}

	public String getMdcCd() {
		return mdcCd;
	}

	public void setMdcCd(String mdcCd) {
		this.mdcCd = mdcCd;
	}

	public String getMdcAdd() {
		return mdcAdd;
	}

	public void setMdcAdd(String mdcAdd) {
		this.mdcAdd = mdcAdd;
	}

	public Integer getMdcZipcd() {
		return mdcZipcd;
	}

	public void setMdcZipcd(Integer mdcZipcd) {
		this.mdcZipcd = mdcZipcd;
	}

	public Integer getMdcTel() {
		return mdcTel;
	}

	public void setMdcTel(Integer mdcTel) {
		this.mdcTel = mdcTel;
	}

	public String getRegUser() {
		return regUser;
	}

	public void setRegUser(String regUser) {
		this.regUser = regUser;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public String getUpdUser() {
		return updUser;
	}

	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	public Date getUpdDate() {
		return updDate;
	}

	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
}
