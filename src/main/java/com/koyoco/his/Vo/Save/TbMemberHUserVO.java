package com.koyoco.his.Vo.Save;

import java.sql.Date;

public class TbMemberHUserVO {
	
	// 회원가입 이력
	private Integer hisSeq;
	private String userId;
	private String gblId;
	private String regUser;
	private Date regDate;

	public Integer getHisSeq() {
		return hisSeq;
	}

	public void setHisSeq(Integer hisSeq) {
		this.hisSeq = hisSeq;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getGblId() {
		return gblId;
	}

	public void setGblId(String gblId) {
		this.gblId = gblId;
	}

	public String getRegUser() {
		return regUser;
	}

	public void setRegUser(String regUser) {
		this.regUser = regUser;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}
}
