package com.koyoco.his.Vo.Save;

import java.sql.Date;

public class TbMemberSKakaoUserVO {
	
	// 카카오 사용자 인증
	private String userId;
	private String gblId;
	private String authKey;
	private String kakaoRcv;
	private String regUser;
	private Date regDate;
	private String updUser;
	private Date updDate;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getGblId() {
		return gblId;
	}

	public void setGblId(String gblId) {
		this.gblId = gblId;
	}

	public String getAuthKey() {
		return authKey;
	}

	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}

	public String getKakaoRcv() {
		return kakaoRcv;
	}

	public void setKakaoRcv(String kakaoRcv) {
		this.kakaoRcv = kakaoRcv;
	}

	public String getRegUser() {
		return regUser;
	}

	public void setRegUser(String regUser) {
		this.regUser = regUser;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public String getUpdUser() {
		return updUser;
	}

	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	public Date getUpdDate() {
		return updDate;
	}

	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
}
