package com.koyoco.his.Vo.find;

import java.util.Date;

public class MemberDetailUserVO {

    private String userId;
    private String userNm;
    private String userPw;
    private String userRcvEmail;
    private String userSeqNum1;
    private String userSeqNum2;
    private String userPhone;
    private String userStatusCd;
    private String userEmail;
    private Integer userPwCnt;
    private String userNationCd;
    private String userNationNm;
    private String userNationAbbrvNm;
    private String userSmsYn;
    private String regUser;
    private Date regDt;
    private String updUser;
    private Date updDt;
    private String userDelYn;
    private Date userDelDt;

    private Integer businessCount;
    private Integer medicalCount;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    public String getUserNm() {
        return userNm;
    }

    public void setUserNm(String userNm) {
        this.userNm = userNm;
    }

    public String getUserPw() {
        return userPw;
    }

    public void setUserPw(String userPw) {
        this.userPw = userPw;
    }

    public String getUserRcvEmail() {
        return userRcvEmail;
    }

    public void setUserRcvEmail(String userRcvEmail) {
        this.userRcvEmail = userRcvEmail;
    }

    public String getUserSeqNum1() {
        return userSeqNum1;
    }

    public void setUserSeqNum1(String userSeqNum1) {
        this.userSeqNum1 = userSeqNum1;
    }

    public String getUserSeqNum2() {
        return userSeqNum2;
    }

    public void setUserSeqNum2(String userSeqNum2) {
        this.userSeqNum2 = userSeqNum2;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserStatusCd() {
        return userStatusCd;
    }

    public void setUserStatusCd(String userStatusCd) {
        this.userStatusCd = userStatusCd;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Integer getUserPwCnt() {
        return userPwCnt;
    }

    public void setUserPwCnt(Integer userPwCnt) {
        this.userPwCnt = userPwCnt;
    }

    public String getUserNationCd() {
        return userNationCd;
    }

    public void setUserNationCd(String userNationCd) {
        this.userNationCd = userNationCd;
    }

    public String getUserNationNm() {
        return userNationNm;
    }

    public void setUserNationNm(String userNationNm) {
        this.userNationNm = userNationNm;
    }

    public String getUserNationAbbrvNm() {
        return userNationAbbrvNm;
    }

    public void setUserNationAbbrvNm(String userNationAbbrvNm) {
        this.userNationAbbrvNm = userNationAbbrvNm;
    }

    public String getUserSmsYn() {
        return userSmsYn;
    }

    public void setUserSmsYn(String userSmsYn) {
        this.userSmsYn = userSmsYn;
    }

    public String getRegUser() {
        return regUser;
    }

    public void setRegUser(String regUser) {
        this.regUser = regUser;
    }

    public Date getRegDt() {
        return regDt;
    }

    public void setRegDt(Date regDt) {
        this.regDt = regDt;
    }

    public String getUpdUser() {
        return updUser;
    }

    public void setUpdUser(String updUser) {
        this.updUser = updUser;
    }

    public Date getUpdDt() {
        return updDt;
    }

    public void setUpdDt(Date updDt) {
        this.updDt = updDt;
    }

    public String getUserDelYn() {
        return userDelYn;
    }

    public void setUserDelYn(String userDelYn) {
        this.userDelYn = userDelYn;
    }

    public Date getUserDelDt() {
        return userDelDt;
    }

    public void setUserDelDt(Date userDelDt) {
        this.userDelDt = userDelDt;
    }

    public Integer getBusinessCount() {
        return businessCount;
    }

    public void setBusinessCount(Integer businessCount) {
        this.businessCount = businessCount;
    }

    public Integer getMedicalCount() {
        return medicalCount;
    }

    public void setMedicalCount(Integer medicalCount) {
        this.medicalCount = medicalCount;
    }

    @Override
    public String toString() {
        return "MemberDetailUserVO{" +
                "userId='" + userId + '\'' +
                ", userNm='" + userNm + '\'' +
                ", userPw='" + userPw + '\'' +
                ", userRcvEmail='" + userRcvEmail + '\'' +
                ", userSeqNum1='" + userSeqNum1 + '\'' +
                ", userSeqNum2='" + userSeqNum2 + '\'' +
                ", userPhone='" + userPhone + '\'' +
                ", userStatusCd='" + userStatusCd + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", userPwCnt=" + userPwCnt +
                ", userNationCd='" + userNationCd + '\'' +
                ", userNationNm='" + userNationNm + '\'' +
                ", userNationAbbrvNm='" + userNationAbbrvNm + '\'' +
                ", userSmsYn='" + userSmsYn + '\'' +
                ", regUser='" + regUser + '\'' +
                ", regDt=" + regDt +
                ", updUser='" + updUser + '\'' +
                ", updDt=" + updDt +
                ", userDelYn='" + userDelYn + '\'' +
                ", userDelDt=" + userDelDt +
                ", businessCount=" + businessCount +
                ", medicalCount=" + medicalCount +
                '}';
    }
}
