package com.koyoco.his.Vo.find;

import java.sql.Date;

public class BoardThesisDetailVO {

	// 논문 게시글 조회
	private Integer THESIS_SEQ;
	private String tUSER_ID;
	private String THESIS_TITLE;
	private String LANG_CD;
	private String THESIS_YEAR;
	private String THESIS_AUTHOR;
	private Integer THESIS_PRICE;
	private Integer DOWN_CNT;
	private Integer VIEW_CNT;
	private String AUTHOR_INTRO;
	private String DOCU_INTRO;
	private String ADD_INFO;
	private String REG_USER;
	private Date REG_DATE;
	private String UPD_USER;
	private Date UPD_DATE;
	
	
	public Integer getTHESIS_SEQ() {
		return THESIS_SEQ;
	}
	public void setTHESIS_SEQ(Integer tHESIS_SEQ) {
		THESIS_SEQ = tHESIS_SEQ;
	}
	public String gettUSER_ID() {
		return tUSER_ID;
	}
	public void settUSER_ID(String tUSER_ID) {
		this.tUSER_ID = tUSER_ID;
	}
	public String getTHESIS_TITLE() {
		return THESIS_TITLE;
	}
	public void setTHESIS_TITLE(String tHESIS_TITLE) {
		THESIS_TITLE = tHESIS_TITLE;
	}
	public String getLANG_CD() {
		return LANG_CD;
	}
	public void setLANG_CD(String lANG_CD) {
		LANG_CD = lANG_CD;
	}
	public String getTHESIS_YEAR() {
		return THESIS_YEAR;
	}
	public void setTHESIS_YEAR(String tHESIS_YEAR) {
		THESIS_YEAR = tHESIS_YEAR;
	}
	public String getTHESIS_AUTHOR() {
		return THESIS_AUTHOR;
	}
	public void setTHESIS_AUTHOR(String tHESIS_AUTHOR) {
		THESIS_AUTHOR = tHESIS_AUTHOR;
	}
	public Integer getTHESIS_PRICE() {
		return THESIS_PRICE;
	}
	public void setTHESIS_PRICE(Integer tHESIS_PRICE) {
		THESIS_PRICE = tHESIS_PRICE;
	}
	public Integer getDOWN_CNT() {
		return DOWN_CNT;
	}
	public void setDOWN_CNT(Integer dOWN_CNT) {
		DOWN_CNT = dOWN_CNT;
	}
	public Integer getVIEW_CNT() {
		return VIEW_CNT;
	}
	public void setVIEW_CNT(Integer vIEW_CNT) {
		VIEW_CNT = vIEW_CNT;
	}
	public String getAUTHOR_INTRO() {
		return AUTHOR_INTRO;
	}
	public void setAUTHOR_INTRO(String aUTHOR_INTRO) {
		AUTHOR_INTRO = aUTHOR_INTRO;
	}
	public String getDOCU_INTRO() {
		return DOCU_INTRO;
	}
	public void setDOCU_INTRO(String dOCU_INTRO) {
		DOCU_INTRO = dOCU_INTRO;
	}
	public String getADD_INFO() {
		return ADD_INFO;
	}
	public void setADD_INFO(String aDD_INFO) {
		ADD_INFO = aDD_INFO;
	}
	public String getREG_USER() {
		return REG_USER;
	}
	public void setREG_USER(String rEG_USER) {
		REG_USER = rEG_USER;
	}
	public Date getREG_DATE() {
		return REG_DATE;
	}
	public void setREG_DATE(Date rEG_DATE) {
		REG_DATE = rEG_DATE;
	}
	public String getUPD_USER() {
		return UPD_USER;
	}
	public void setUPD_USER(String uPD_USER) {
		UPD_USER = uPD_USER;
	}
	public Date getUPD_DATE() {
		return UPD_DATE;
	}
	public void setUPD_DATE(Date uPD_DATE) {
		UPD_DATE = uPD_DATE;
	}
	
	
}
