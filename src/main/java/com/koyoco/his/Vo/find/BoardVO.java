package com.koyoco.his.Vo.find;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class BoardVO {

	// 게시글 조회
	private Integer thesisSeq;
	private String userId;
	private String title;
	private String year;
	private String author;
	private Integer price;
	private Integer dowbCnt;
	private Integer viewCnt;
	private String authorIntro;
	private String docuIntro;
	private String addInfo;
	private String regUser;

	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date regDate;
	private String updUser;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date updDate;
	private String flag;

	public Integer getThesisSeq() {
		return thesisSeq;
	}

	public void setThesisSeq(Integer thesisSeq) {
		this.thesisSeq = thesisSeq;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getDowbCnt() {
		return dowbCnt;
	}

	public void setDowbCnt(Integer dowbCnt) {
		this.dowbCnt = dowbCnt;
	}

	public Integer getViewCnt() {
		return viewCnt;
	}

	public void setViewCnt(Integer viewCnt) {
		this.viewCnt = viewCnt;
	}

	public String getAuthorIntro() {
		return authorIntro;
	}

	public void setAuthorIntro(String authorIntro) {
		this.authorIntro = authorIntro;
	}

	public String getDocuIntro() {
		return docuIntro;
	}

	public void setDocuIntro(String docuIntro) {
		this.docuIntro = docuIntro;
	}

	public String getAddInfo() {
		return addInfo;
	}

	public void setAddInfo(String addInfo) {
		this.addInfo = addInfo;
	}

	public String getRegUser() {
		return regUser;
	}

	public void setRegUser(String regUser) {
		this.regUser = regUser;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}


	public String getUpdUser() {
		return updUser;
	}

	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	public Date getUpdDate() {
		return updDate;
	}

	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

}
