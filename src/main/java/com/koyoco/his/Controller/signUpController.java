package com.koyoco.his.Controller;

import com.koyoco.his.Service.SignUpService;
import com.koyoco.his.Vo.find.MemberSearchUserVO;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/signUp")
public class signUpController {
	
	   @Autowired
	   private SignUpService signService;
	   
	   @RequestMapping("/signUpCheck")
	   public ModelAndView SignUpCheck () {
	   	   
	   	   ModelAndView v = new ModelAndView();
	   	   v.setViewName("../signUp/signUpCheck");
	   	   return v;
	   }

	   @RequestMapping("/signUp")
	   public ModelAndView SignUpView () {
	   	   
		   MemberSearchUserVO memberSearchUserVO = new MemberSearchUserVO();
		   
	   	   ModelAndView v = new ModelAndView();
	       v.addObject("signUp", memberSearchUserVO);
	       v.setViewName("../signUp/signUpView");
	       return v;
	   	   
	   }
	   
	   @RequestMapping(value = "/signUpEnd", method = RequestMethod.POST)
	   public @ResponseBody HashMap<String, Object> SignUpEnd (@RequestBody HashMap<String, Object> req, HttpServletResponse res) {
	   	   
		   req.put("rtn", signService.signUpSave(req));
	   	   
	   	   return req;
	   }
	   
	   @RequestMapping("/login")
	   public ModelAndView login() {
	   	   
	   	   ModelAndView v = new ModelAndView();
	   	   MemberSearchUserVO memberSearchUserVO = new MemberSearchUserVO();
	       v.addObject("signUp", memberSearchUserVO);
	       v.setViewName("../signUp/login");
	       return v;
	   }
	   
	   @RequestMapping(value = "/loginEnd", method = RequestMethod.POST)
	   public @ResponseBody List<MemberSearchUserVO> LoginEnd (@RequestBody HashMap<String, Object> req, HttpSession session) {
	   	   
		   
		   List<MemberSearchUserVO> memberSearchUserVO = signService.loginCheck(req);
	   	    if (memberSearchUserVO != null) {
		   	    session.setAttribute("userId",req.get("userId"));
		        session.setMaxInactiveInterval(-1);
	   	    }
	   	    return memberSearchUserVO;
	   }

}
