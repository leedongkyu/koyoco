package com.koyoco.his.Controller;

import com.koyoco.his.Service.BoardService;
import com.koyoco.his.Service.MemberService;
import com.koyoco.his.Vo.Save.TbMemberMUserVO;
import com.koyoco.his.Vo.find.MemberDetailUserVO;
import com.koyoco.his.Vo.find.MemberTransformVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

import static com.koyoco.his.Common.ConvertUtil.convertToMap;

@Controller
@RequestMapping("/member")
public class MemberController {
    @Autowired
    private MemberService memberService;

    @GetMapping(value = "/detail/user")
    public ModelAndView infoView(HttpSession session){

        ModelAndView modelAndView = new ModelAndView();
        Map<String,Object> map = new HashMap<>();
        map.put("userId",session.getAttribute("userId"));


        MemberDetailUserVO memberDetailUserVO = memberService.findUser(map);

        modelAndView.addObject("member", memberDetailUserVO);
        modelAndView.setViewName("../member/memberDetail");
        return modelAndView;
    }

    @PostMapping(value = "/detail/user/update")
    @ResponseBody
    public Map<String,Object> infoUpdate(@RequestBody TbMemberMUserVO param){

        Map<String,Object> rtnMap= new HashMap<>();
        System.out.println("param" +param.getUserEmail());

        memberService.updateInfo(param);

        rtnMap.put("rtn","success");
        return rtnMap;
    }
    @GetMapping(value = "/detail/user/transForm")
    public ModelAndView infoTransFormView(HttpSession session){

        ModelAndView modelAndView = new ModelAndView();
        String userId = session.getAttribute("userId").toString();
        Map<String,Object> map = new HashMap<>();

        map.put("userId",userId);

        MemberTransformVO memberTransformVO =memberService.findTrfUser(map);
        modelAndView.addObject("memberTrf", memberTransformVO);
        modelAndView.setViewName("../member/memberTransform");
        return modelAndView;
    }

    @PostMapping(value = "/detail/user/transInsert")
    @ResponseBody
    public Map<String,Object> transFormUpdate(@RequestBody MemberTransformVO param,HttpSession session){
        String userId = session.getAttribute("userId").toString();
        Map<String,Object> rtnMap= new HashMap<>();
        System.out.println("param : " +param.getSeqNum());
        param.setUserId(userId);
        memberService.transInsert(param);

        rtnMap.put("rtn","success");
        return rtnMap;
    }

}
