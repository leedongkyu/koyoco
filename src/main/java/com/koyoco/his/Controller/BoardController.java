package com.koyoco.his.Controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.plexus.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.koyoco.his.Service.BoardService;
import com.koyoco.his.Vo.find.BoardThesisDetailVO;
import com.koyoco.his.Vo.find.BoardVO;

@Controller
@RequestMapping("/board")
public class BoardController {
	
	@Autowired
    private BoardService boardService;

	// 논문 리스트 페이지
	@RequestMapping(value = "/thesis/page")
    public ModelAndView thesis(String title, HttpServletRequest req, HttpServletResponse res){

		ModelAndView modelAndView = new ModelAndView();
        Map<String, Object> param = new HashMap<String, Object>();
        if (StringUtils.isNotEmpty(title)) {
        	param.put("TITLE", title);
            List<BoardVO> boardVO = boardService.search(param);
            modelAndView.addObject("boardCount", boardVO.size());
            modelAndView.addObject("board", boardVO);
            modelAndView.addObject("title", title);
        }
        modelAndView.setViewName("../board/thesis");
        return modelAndView;
    }
	
	// 논문 리스트 검색
	@RequestMapping(value = "/thesis/search")
	@ResponseBody
    public List<BoardVO> thesisSearch(@RequestBody Map<String,Object> param
    		,HttpServletRequest req, HttpServletResponse res){
		
		List<BoardVO> boardThesisDetailVO = boardService.search(param);
    	return boardThesisDetailVO;
	}
	
	// 논문 등록 페이지
	@RequestMapping(value = "/thesis/register/page")
    public ModelAndView thesisRegister(){

        ModelAndView modelAndView = new ModelAndView();
        BoardThesisDetailVO boardThesisDetailVO = new BoardThesisDetailVO();
        modelAndView.addObject("board", boardThesisDetailVO);
        modelAndView.setViewName("../board/thesisRegister");
        return modelAndView;
    }
	
	// 등록
	@RequestMapping(value = "/thesis/register/save")
	@ResponseBody
    public Map<String,Object> thesisRegisterSave(@RequestBody Map<String,Object> param
    		   ,HttpServletRequest req, HttpServletResponse res){
		param.put("rtn", boardService.registerSave(param));
		return param;
    }
	
	// 논문 상세페이지
	@RequestMapping(value = "/thesis/detail/page")
    public ModelAndView thesisDetail(){
		Map<String, Object> param = new HashMap<String, Object>();
        ModelAndView modelAndView = new ModelAndView();
        BoardThesisDetailVO boardThesisDetailVO = boardService.detailSearch(param);
        modelAndView.addObject("board", boardThesisDetailVO);
        modelAndView.setViewName("../board/thesisDetail");
        return modelAndView;
    }
	
	// 논문 상세페이지
	@RequestMapping(value = "/thesis/detail/search")
	@ResponseBody
    public List<BoardThesisDetailVO> thesisDetailSearch(@RequestBody Map<String,Object> param
    		   ,HttpServletRequest req, HttpServletResponse res){
        try {
        	/*
        	JSONObject jsonObject = new JSONObject();
        	List<BoardThesisDetailVO> boardThesisDetailVO = boardService.search(param);
        	jsonObject.put("date", boardThesisDetailVO);
        	PrintWriter po = res.getWriter();
        	po.print(jsonObject.toString()); 
        	*/
//        	List<BoardThesisDetailVO> boardThesisDetailVO = boardService.search(param);
//        	return boardThesisDetailVO;
        	return null;
        } catch (Exception e) {
        	System.out.println(e);
        	return null;
		}
    }
}
