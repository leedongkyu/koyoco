package com.koyoco.his.Controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.koyoco.his.Service.TestService;
import com.koyoco.his.Vo.CountSearch;

@Controller
public class MainController {
   //private static final Logger logger = LoggerFactory.getLogger(MemberController.class);
   
    @Autowired
    private TestService testService;

   @RequestMapping(value = "/Admin/main", method = RequestMethod.GET)
   public ModelAndView Main(@RequestParam(value = "error", required = false) String error,@RequestParam(value = "logout", required = false) String logout)  {
      ModelAndView v = new ModelAndView();
      v.setViewName("../spample/index");
      return v;
   }
   
   @RequestMapping(value = "/Admin/NewFile", method = RequestMethod.GET)
   public ModelAndView NewFile(@RequestParam(value = "error", required = false) String error,@RequestParam(value = "logout", required = false) String logout)  {
      ModelAndView v = new ModelAndView();
      v.setViewName("../spample/NewFile");
      return v;
   }
   
   @RequestMapping(value = "/Admin/chartTest", method = RequestMethod.GET)
	public ModelAndView chartTest()  {
		ModelAndView v = new ModelAndView();
		v.setViewName("../spample/chartTest");
		return v;
	}

   @RequestMapping("/Admin/movePage")
   public String movePage() {
	   return "../spample/index2";
   }
   
   @RequestMapping("/Admin/Login")
   public ModelAndView Login() {
       ModelAndView v = new ModelAndView();
       v.setViewName("../spample/login");
       return v;
   }
   
   //로딩바
   @RequestMapping("/Admin/layout/07_loading")
   public String loading() {
	   return "../spample/layout/07-loading";
   }
   
   //로딩바
   @RequestMapping("/Admin/layout/07-loading-function")
   public String loading_function() {
	   return "../spample/layout/07-loading";
   }
   
   @RequestMapping("/Admin/layout/BOLOL10002")
   public String BOLOL10002() {
	   return "../spample/layout/BOLOL10002";
   }
   
   //레이아웃
   @RequestMapping("/Admin/layout/layout")
   public String layout() {
	   return "../layout/header";
   }
   
   //레이아웃1
   @RequestMapping("/Admin/layout/layout1")
   public String layout1() {
	   return "../layout/header_search";
   }
   
   //list
   @RequestMapping("/Admin/spample/list")
   public String list() {
	   return "../spample/00-smaple-page";
   }

   //serialize 
   @RequestMapping("/Admin/serializeTest")
   public String serialize() {
	   return "../spample/serializeTest";
   }
   
   // 로직 구현을 위한 테스트
   @RequestMapping(value = "/AllTest/{id}", method = RequestMethod.GET)
      public ModelAndView Result(@PathVariable("map") Map<String, Object> map, Model model) {
      ModelAndView view = new ModelAndView();
      view.setViewName("/AllTest");
      System.out.println("동규바보");
      CountSearch test = testService.selectId(map);

      model.addAttribute("Id" , test);
      return view;
   }

   @RequestMapping(value = "/AllTests/{id}", method = RequestMethod.GET)
   public ModelAndView TestResult(@PathVariable("map") Map<String, Object> map, Model model) {
      ModelAndView view = new ModelAndView();
      view.setViewName("/AllTest");
      System.out.println("동규바보1");
      List<CountSearch> test = testService.selectIds(map);


      System.out.println("동규 멍청이 1: " + test.get(0).getSeq());
      System.out.println("동규 멍청이 2: " + test.get(0).gettName());
      model.addAttribute( "Id" , test);
      return view;
   }
   
}