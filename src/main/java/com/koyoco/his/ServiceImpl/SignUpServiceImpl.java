package com.koyoco.his.ServiceImpl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.koyoco.his.Dao.CommonDao;
import com.koyoco.his.Service.SignUpService;
import com.koyoco.his.Vo.find.MemberSearchUserVO;

@Repository
public class SignUpServiceImpl implements SignUpService {
	@Autowired
   private CommonDao commonDao;
	
	@Override
	public String signUpSave(Map<String, Object> map) {
		int data =  commonDao.insert("SignUpMapper.SignUpSave",map);
		if (data == 1) {
			return "success";
		} else {
			return "fail";
		}
	}
	
	@Override
    public List<MemberSearchUserVO> loginCheck(Map<String,Object> map) {
		
		ObjectMapper objectMapper = new ObjectMapper();
		List<Map<String, Object>> data = commonDao.selectList("SignUpMapper.loginCheck",map);
		try {
			List<MemberSearchUserVO> memberSearchUserVO = objectMapper.convertValue(data, new TypeReference<List<MemberSearchUserVO>> (){});
			return memberSearchUserVO;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
    }
}
