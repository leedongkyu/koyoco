package com.koyoco.his.ServiceImpl;

import java.util.List;
import java.util.Map;

import com.koyoco.his.Common.ConvertUtil;
import com.koyoco.his.Service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.koyoco.his.Dao.CommonDao;
import com.koyoco.his.Vo.CountSearch;

@Repository
public class TestServiceImpl implements TestService {

//   @Autowired
//   private SqlSessionTemplate sqlTemplate;
  
   @Autowired
   private CommonDao testDao;

   @Override
   public CountSearch selectId(Map<String, Object> map) {
      Map<String,Object> a =  testDao.selectOne("TestMapper.selectReadTest",map);
      CountSearch countSearch = new CountSearch();
      try {
         countSearch = ConvertUtil.convertToValueObject(a, CountSearch.class);
      }catch (Exception e){
         System.out.println("동규 바보");
      }
      return countSearch;
   }

   @Override
   public List<CountSearch> selectIds(Map<String, Object> map) {
      List<Map<String, Object>> countSearchList =  testDao.selectList("TestMapper.selectReadTest", map);
      try {
         return ConvertUtil.convertToValueObjects(countSearchList, CountSearch.class);
      }catch (Exception e){
         System.out.println("동규 바보");
         e.printStackTrace();
         return null;
      }
   }
}