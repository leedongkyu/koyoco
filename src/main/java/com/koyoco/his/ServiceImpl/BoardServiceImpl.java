package com.koyoco.his.ServiceImpl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.koyoco.his.Dao.CommonDao;
import com.koyoco.his.Service.BoardService;
import com.koyoco.his.Vo.find.BoardThesisDetailVO;
import com.koyoco.his.Vo.find.BoardVO;

@Repository
public class BoardServiceImpl implements BoardService {
	@Autowired
   private CommonDao commonDao;

	@Override
	public List<BoardVO> search(Map<String, Object> param) {
		ObjectMapper objectMapper = new ObjectMapper();
		List<Map<String, Object>> data = commonDao.selectList("BoardMapper.search",param);
		try {
			List<BoardVO> boardVO = objectMapper.convertValue(data, new TypeReference<List<BoardVO>> (){});
			return boardVO;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public BoardThesisDetailVO detailSearch(Map<String, Object> param) {
		ObjectMapper objectMapper = new ObjectMapper();
		if ("thesis".equals(param.get("division"))) {
			// 논문 게시글 상세
			List<Map<String, Object>> data = commonDao.selectList("BoardMapper.thesisDetailSearch",param);
			BoardThesisDetailVO boardThesisDetailVO = objectMapper.convertValue(data, BoardThesisDetailVO.class);
			return boardThesisDetailVO;
		} else {
			// 학술/세미나 게시글 상세
			List<Map<String, Object>> data = commonDao.selectList("BoardMapper.academicDetailSearch",param);
			BoardThesisDetailVO boardThesisDetailVO = objectMapper.convertValue(data, BoardThesisDetailVO.class);
			return boardThesisDetailVO;
		}
	}

	@Override
	public String registerSave(Map<String, Object> map) {
		System.out.println(map);
		int data =  commonDao.insert("BoardMapper.registerSave",map);
		if (data == 1) {
			return "success";
		} else {
			return "fail";
		}
	}
}
