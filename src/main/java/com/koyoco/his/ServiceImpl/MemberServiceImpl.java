package com.koyoco.his.ServiceImpl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.koyoco.his.Common.ConvertUtil;
import com.koyoco.his.Dao.CommonDao;
import com.koyoco.his.Service.MemberService;
import com.koyoco.his.Vo.Save.TbMemberMUserVO;
import com.koyoco.his.Vo.find.MemberDetailUserVO;
import com.koyoco.his.Vo.find.MemberTransformVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.koyoco.his.Common.ConvertUtil.convertToMap;
import static com.koyoco.his.Common.ConvertUtil.convertToValueObject;

@Repository
public class MemberServiceImpl implements MemberService {

    @Autowired
    private CommonDao commonDao;


    @Override
    public String updateInfo(TbMemberMUserVO tbMemberMUserVO) {
        int data = 0;
        try {
            Map<String, Object> map = convertToMap(tbMemberMUserVO);
            data =  commonDao.update("MemberMapper.updateUserInfo",map);
        }catch (Exception e){
            System.out.println("SQL ERROR" + e);
        }

        if (data == 1) {
            return "success";
        } else {
            return "fail";
        }
    }

    @Override
    public MemberDetailUserVO findUser(Map<String,Object> map) {
        Map<String, Object> rtnMap = new HashMap<>();
        MemberDetailUserVO data= null;
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            rtnMap =  commonDao.selectOne("MemberMapper.selectUser",map);
            data = objectMapper.convertValue(rtnMap,MemberDetailUserVO.class);

            System.out.println(ConvertUtil.convertToMap(data));
        }catch (Exception e){
            System.out.println("SQL ERROR" + e);
        }

        return data;
    }
    @Override
    public MemberTransformVO findTrfUser(Map<String,Object> map) {
        Map<String, Object> rtnMap = new HashMap<>();
        MemberTransformVO data= null;
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            rtnMap =  commonDao.selectOne("MemberMapper.selectUser",map);
            data = objectMapper.convertValue(rtnMap,MemberTransformVO.class);

            System.out.println(ConvertUtil.convertToMap(data));
        }catch (Exception e){
            System.out.println("SQL ERROR" + e);
        }

        return data;
    }

    @Override
    public String transInsert(MemberTransformVO memberTransformVO) {
        int data = 0;
        String sqlUrl = null;
        if("1".equals(memberTransformVO.getSeqNum())){
            sqlUrl = "MemberMapper.insertMedical";
        }else{
            sqlUrl = "MemberMapper.insertBusiness";
        }


        try {
            Map<String, Object> map = convertToMap(memberTransformVO);
            data =  commonDao.insert(sqlUrl,map);
        }catch (Exception e){
            System.out.println("SQL ERROR" + e);
        }

        if (data == 1) {
            return "success";
        } else {
            return "fail";
        }
    }

}
