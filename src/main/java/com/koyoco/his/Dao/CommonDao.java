package com.koyoco.his.Dao;

import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class CommonDao {
   
     @Autowired
     private SqlSessionTemplate sqlSessionTemplate;
//

     public Map<String, Object>  selectOne(String statement, Map<String, Object> map) {
         Map<String,Object> mapList =  sqlSessionTemplate.selectOne(statement, map);
         return mapList;

     }
    
    public List<Map<String, Object>> selectList(String statement, Map<String, Object> map) {
        List<Map<String,Object>> mapList =  sqlSessionTemplate.selectList(statement, map);
        return mapList;
    }
    
    public int insert(String statement, Map<String, Object> map) {
    	int insert =  sqlSessionTemplate.insert(statement, map);
    	return insert;
    }
    
    public int update(String statement, Map<String, Object> map) {
    	int insert =  sqlSessionTemplate.update(statement, map);
    	return insert;
    }
}
