<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<!-- div 왼쪽, 오른쪽 바깥여백을 auto로 주면 중앙정렬된다.  -->
    <div id="wrap">
        <br><br>
        <b><font size="6" color="gray" text-align = "center">회원가입</font></b>
        <br><br><br>
        
        
        <!-- 입력한 값을 전송하기 위해 form 태그를 사용한다 -->
        <!-- 값(파라미터) 전송은 POST 방식, 전송할 페이지는 JoinPro.jsp -->
        <form method="post" action="../pro/JoinPro.jsp" name="userInfo" 
                onsubmit="return checkValue()">
            <table>
                <tr>
                    <td id="title">아이디</td>
                    <td>
                        <input type="text" name="id" maxlength="50">
                        <input type="button" value="중복확인" >    
                    </td>
                </tr>
                        
                <tr>
                    <td id="title">비밀번호</td>
                    <td>
                        <input type="password" name="password" maxlength="50">
                    </td>
                </tr>
                
                <tr>
                    <td id="title">비밀번호 확인</td>
                    <td>
                        <input type="password" name="passwordcheck" maxlength="50">
                    </td>
                </tr>
                    
                <tr>
                    <td id="title">이름</td>
                    <td>
                        <input type="text" name="name" maxlength="50">
                    </td>
                </tr>
                <tr>
                    <td id="title">이메일</td>
                    <td>
                        <input type="text" name="mail1" maxlength="50">@
                        <select name="mail2">
                            <option>naver.com</option>
                            <option>daum.net</option>
                            <option>gmail.com</option>
                            <option>nate.com</option>                        
                        </select>
                    </td>
                </tr>
                    
                <tr>
                    <td id="title">휴대전화</td>
                    <td>
                        <input type="text" name="phone" />
                    </td>
                </tr>
                <tr>
                    <td id="title">추천인</td>
                    <td>
                        <input type="text" name="name" maxlength="50">
                    </td>
                </tr>
            </table>
            <br>
            <input type="submit" value="가입"/>  
            <input type="button" value="취소" onclick="goLoginForm()">
        </form>
    </div>
</body>
<script>

</script>
	<!-- 컨텐츠 영역 --> 
	<main id="doc-contents">
		<div class="signup-wrap">
			<h2 class="signup-tit">간편가입</h2>
			
			<article class="signup-box2">
				
				<div class="signup-btns_sns">
					<a href="#" onClick="javascript:window.open('https://nid.naver.com/oauth2.0/authorize?response_type&#x3D;code&amp;client_id&#x3D;lKFdFcYB3_zG0kl3EjH6&amp;redirect_uri&#x3D;https://www.gsshop.com/cust/login/chkNaverLogin.gs&amp;state&#x3D;JOIN_l65k4avantp4bu6qli43nb5iho', 'snslogin', 'width=500, height=700, resizable=1, scrollbars=yes, status=0, titlebar=0, toolbar=0, left=435, top=250');return false;" class="gui-btn huge"><i class="sprite-naver"></i><strong>네이버로 쉬운 가입</strong></a>
					<a href="#" onClick="javascript:window.open('https://kauth.kakao.com/oauth/authorize?response_type&#x3D;code&amp;client_id&#x3D;009866fb7881d62f94177d2988825a75&amp;redirect_uri&#x3D;https://www.gsshop.com/cust/login/chkKakaoLogin.gs&amp;state&#x3D;JOIN_l65k4avantp4bu6qli43nb5iho', 'snslogin', 'width=500, height=700, resizable=1, scrollbars=yes, status=0, titlebar=0, toolbar=0, left=435, top=250');return false;" class="gui-btn huge"><i class="sprite-kakao"></i><strong>카카오로 쉬운 가입</strong></a>
					<a href="/cust/adm/admForm.gs" class="gui-btn huge"><i class="sprite-email"></i><strong>이메일로 가입</strong></a>
				</div>
			</article>

		</div><!--// signup-wrap -->
	</main>
</html>