<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--
/*************************************************************************
* @ 서비스경로 : Guide > Sample > Layout
* @ 파일명    : 01-layout.html
* @ 작성자    : joar
* @ 작성일    :2022-01-22
************************** 수정이력 ****************************************
* 날짜                    작업자                 변경내용
*_________________________________________________________________________
* 2022-01-22             joar                 최초작성
*************************************************************************/
-->
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<title>코요코</title>
<jsp:include page="/WEB-INF/layout/header_search.jsp"></jsp:include>
</head>
<body>
<div id="view-wrap" aria-hidden="false" class="search-box"> <!-- view-wrap S -->
<div id="content-wrap"> <!-- content-wrap S -->
    <div id="content">
        <div class="popularity-box">
            <span>인기검색어</span>
            <a href="javascript:;"><span>인기검색어1</span></a>
            <a href="javascript:;"><span>인기검색어2</span></a>
            <a href="javascript:;"><span>인기검색어3</span></a>
            <a href="javascript:;"><span>인기검색어4</span></a>
        </div>
        <div class="cmm-tab">
            <a class="active" href="javascript:;"><span>전체</span></a>
            <a href="javascript:;"><span>논문</span></a>
            <a href="javascript:;"><span>저자</span></a>
            <a href="javascript:;"><span>메뉴얼</span></a>
        </div>
        <h1 class="page-tit">검색결과<span class="num">230</span></h1>
        <div class="con-box">
            <h2>논문 (<span class="con-num">10</span>)</h2>
            <button class="btn-icon plus"><span>더보기</span></button>
            <div class="con-list">
                <p>문서 제목이 들어갑니다. 문서 제목이 들어갑니다. 문서 제목이 들어갑니다. 문서 제목이 들어갑니다. 문서 제목이 들어갑니다.
                문서 제목이 들어갑니다.문서 제목이 들어갑니다.문서 제목이 들어갑니다.문서 제목이 들어갑니다.문서 제목이 들어갑니</p>
                <div class="cmm-flex">
                    <strong>저자 <span class="con-name">최준호</span> 등록자 <span class="con-name2">바보</span> 등록일 <span class="con-date">2022.01.22</span> 가격 <span class="con-price">&#8361;2,000,000</span></strong>
                    <label class="checkbox like">
                        <input type="checkbox">
                    </label>
                    <button class="btn-icon download"><span>다운로드</span></button>
                </div>
            </div>
            <div class="con-list">
                <p>문서 제목이 들어갑니다. 문서 제목이 들어갑니다. 문서 제목이 들어갑니다. 문서 제목이 들어갑니다. 문서 제목이 들어갑니다.
                문서 제목이 들어갑니다.문서 제목이 들어갑니다.문서 제목이 들어갑니다.문서 제목이 들어갑니다.문서 제목이 들어갑니</p>
                <div class="cmm-flex">
                    <strong>저자 <span class="con-name">최준호</span> 등록자 <span class="con-name2">바보</span> 등록일 <span class="con-date">2022.01.22</span> 가격 <span class="con-price">&#8361;2,000,000</span></strong>
                    <label class="checkbox like">
                        <input type="checkbox">
                    </label>
                    <button class="btn-icon download"><span>다운로드</span></button>
                </div>
            </div>
        </div>

        <div class="con-box">
            <h2>저자 (<span class="con-num">10</span>)</h2>
            <button class="btn-icon plus"><span>더보기</span></button>
            <div class="con-list">
                <p>문서 제목이 들어갑니다. 문서 제목이 들어갑니다. 문서 제목이 들어갑니다. 문서 제목이 들어갑니다. 문서 제목이 들어갑니다.
                문서 제목이 들어갑니다.문서 제목이 들어갑니다.문서 제목이 들어갑니다.문서 제목이 들어갑니다.문서 제목이 들어갑니</p>
                <div class="cmm-flex">
                    <strong>저자 <span class="con-name">최준호</span> 등록자 <span class="con-name2">바보</span> 등록일 <span class="con-date">2022.01.22</span> 가격 <span class="con-price">&#8361;2,000,000</span></strong>
                    <label class="checkbox like">
                        <input type="checkbox">
                    </label>
                    <button class="btn-icon download"><span>다운로드</span></button>
                </div>
            </div>
            <div class="con-list">
                <p>문서 제목이 들어갑니다. 문서 제목이 들어갑니다. 문서 제목이 들어갑니다. 문서 제목이 들어갑니다. 문서 제목이 들어갑니다.
                문서 제목이 들어갑니다.문서 제목이 들어갑니다.문서 제목이 들어갑니다.문서 제목이 들어갑니다.문서 제목이 들어갑니</p>
                <div class="cmm-flex">
                    <strong>저자 <span class="con-name">최준호</span> 등록자 <span class="con-name2">바보</span> 등록일 <span class="con-date">2022.01.22</span> 가격 <span class="con-price">&#8361;2,000,000</span></strong>
                    <label class="checkbox like">
                        <input type="checkbox">
                    </label>
                    <button class="btn-icon download"><span>다운로드</span></button>
                </div>
            </div>
        </div>

        <div class="con-box">
            <h2>메뉴얼 (<span class="con-num">10</span>)</h2>
            <button class="btn-icon plus"><span>더보기</span></button>
            <div class="con-list">
                <p>문서 제목이 들어갑니다. 문서 제목이 들어갑니다. 문서 제목이 들어갑니다. 문서 제목이 들어갑니다. 문서 제목이 들어갑니다.
                문서 제목이 들어갑니다.문서 제목이 들어갑니다.문서 제목이 들어갑니다.문서 제목이 들어갑니다.문서 제목이 들어갑니</p>
                <div class="cmm-flex">
                    <strong>저자 <span class="con-name">최준호</span> 등록자 <span class="con-name2">바보</span> 등록일 <span class="con-date">2022.01.22</span> 가격 <span class="con-price">&#8361;2,000,000</span></strong>
                    <label class="checkbox like">
                        <input type="checkbox">
                    </label>
                    <button class="btn-icon download"><span>다운로드</span></button>
                </div>
            </div>
            <div class="con-list">
                <p>문서 제목이 들어갑니다. 문서 제목이 들어갑니다. 문서 제목이 들어갑니다. 문서 제목이 들어갑니다. 문서 제목이 들어갑니다.
                문서 제목이 들어갑니다.문서 제목이 들어갑니다.문서 제목이 들어갑니다.문서 제목이 들어갑니다.문서 제목이 들어갑니</p>
                <div class="cmm-flex">
                    <strong>저자 <span class="con-name">최준호</span> 등록자 <span class="con-name2">바보</span> 등록일 <span class="con-date">2022.01.22</span> 가격 <span class="con-price">&#8361;2,000,000</span></strong>
                    <label class="checkbox like">
                        <input type="checkbox">
                    </label>
                    <button class="btn-icon download"><span>다운로드</span></button>
                </div>
            </div>
        </div>
    </div>
</div> <!-- content-wrap E -->
</div> <!-- view-wrap E -->


<script type="text/javascript">
    // Name
</script>
</body>
</html>