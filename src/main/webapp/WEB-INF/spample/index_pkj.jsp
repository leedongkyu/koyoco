<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=chrome" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Dashboard - SB Admin</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="css/styles.css" rel="stylesheet" />
		<link href="../../resources/css/styles.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    </head>
	<style>
		/* 가로형 드롭다운 메뉴 */
		.topMenu {
            height: 30px;  
            width: 770px;       /* [변경] 하위 메뉴와 동일하게 맞춤 */
            position: relative;
            background-color: #2D2D2D; /* [추가] 늘어난만큼 배경색도 보이도록 수정 */
	    }
	    .topMenu ul {           /* 메인 메뉴 안의 ul을 설정함: 상위메뉴의 ul+하위 메뉴의 ul */
	        list-style-type: none;  
	        margin: 0px;            
	        padding: 0px;           
	    }
	    .topMenu ul li {            /* 메인 메뉴 안에 ul 태그 안에 있는 li 태그의 스타일 적용(상위/하위메뉴 모두) */
	        color: white;               
	        background-color: #2d2d2d;  
	        float: left;                
	        line-height: 30px;          
	        vertical-align: middle;     
	        text-align: center;         
	        -position: relative;         
	    }
	    .menuLink, .submenuLink {           /* 상위 메뉴와 하위 메뉴의 a 태그에 공통으로 설정할 스타일 */
	        text-decoration:none;               
	        display: block;                     
	        width: 150px;                       
	        font-size: 13px;                    
	        font-weight: bold;                  
	        font-family: "Trebuchet MS", Dotum; 
	        
	    }
	    .menuLink {     /* 상위 메뉴의 글씨색을 흰색으로 설정 */
	        color: white;
	    }
	    .topMenuLi:hover .menuLink {    /* 상위 메뉴의 li에 마우스오버 되었을 때 스타일 설정 */
	        color: red;                 
	        background-color: #4d4d4d;
	    }
	    .longLink {     /* 좀 더 긴 메뉴 스타일 설정 */
	        width: 190px;   
	    }
		.submenuLink {          /* 하위 메뉴의 a 태그 스타일 설정 */
	        color: #2d2d2d;             
	        background-color: #DDD;      /* [변경] 배경색 변경 */
	        -border: solid 1px black;    /* [삭제] 테두리 삭제 */
	        -margin-right: -1px;         /* [삭제] 공백 보정 삭제 */
	    }
	    .submenu {              /* 하위 메뉴 스타일 설정 */
	        position: absolute;     
	        height: 0px;            
	        overflow: hidden;       
	        transition: height .2s; 
	        -webkit-transition: height .2s; 
	        -moz-transition: height .2s; 
	        -o-transition: height .2s; 
	        width: 770px;           
	        left: 0;                
	
	
	        background-color: #DDD; /* [추가] 하위 메뉴 전체에 배경색 설정 */
	    }
	    .submenu li {
	        display: inline-block;
	
	
	    }
	    .topMenuLi:hover .submenu { 
	        height: 32px;           
	    }
	    .submenuLink:hover {        
	        color: red;                 
	        background-color: #dddddd;  
	    }
		
	</style>
    <body class="sb-nav-fixed">
	<!--     
	<nav id="topMenu" > 
		<ul> 
			<li class="topMenuLi"> 
				<a class="menuLink" href="http://unikys.tistory.com/category/Programming%20Lecture">LECTURES</a> 
				<ul class="submenu"> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/%EC%86%8D%EA%B9%8A%EC%9D%80%20%EC%9E%90%EB%B0%94%EC%8A%A4%ED%81%AC%EB%A6%BD%ED%8A%B8%20%EA%B0%95%EC%A2%8C" class="submenuLink longLink">속깊은 자바스크립트 강좌</a></li> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/%EB%B0%91%EB%B0%94%EB%8B%A5%EB%B6%80%ED%84%B0%20%ED%99%88%ED%8E%98%EC%9D%B4%EC%A7%80%20%EB%A7%8C%EB%93%A4%EA%B8%B0" class="submenuLink longLink">밑바닥부터 홈페이지 만들기</a></li> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/Android%28%EC%95%88%EB%93%9C%EB%A1%9C%EC%9D%B4%EB%93%9C%29%20%EC%95%B1%20%EA%B0%9C%EB%B0%9C" class="submenuLink longLink">안드로이드 앱 개발</a></li> 
				</ul> </li> <li>|</li> 
			<li class="topMenuLi"> 
				<a class="menuLink" href="http://unikys.tistory.com/guestbook">GUEST BOOK</a> </li> <li>|</li> 
			<li class="topMenuLi"> 
				<a class="menuLink" href="http://unikys.tistory.com/tag">TAG CLOUD</a> 
				<ul class="submenu"> 
					<li><a href="http://unikys.tistory.com/tag/%EC%9E%90%EB%B0%94%EC%8A%A4%ED%81%AC%EB%A6%BD%ED%8A%B8" class="submenuLink">자바스크립트</a></li> 
					<li><a href="http://unikys.tistory.com/tag/%EA%B0%95%EC%A2%8C" class="submenuLink">강좌</a></li> 
					<li><a href="http://unikys.tistory.com/tag/K100D" class="submenuLink">K100D</a></li> 
				</ul> </li> <li>|</li> 
			<li class="topMenuLi"> 
				<a class="menuLink" href="http://unikys.tistory.com/media">MEDIA LOG </li> <li>|</li> 
			<li class="topMenuLi"> 
				<a class="menuLink" href="http://unikys.tistory.com/location">LOCATION LOG</a> 
			</li> 
		</ul>  
		</nav>
	-->	
       <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="index.html">KOYOCO</a>
            <!-- Sidebar Toggle-->
<!--             <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button> -->
			<a class="navbar-brand ps-3 " href="#" onclick="movePage()">menu1</a>
			<!-- 가로 드롭 메뉴 -->
			<li class="topMenuLi"> 
				<a class="menuLink" href="http://unikys.tistory.com/category/Programming%20Lecture" style="font-size: 1.25rem;">menu2</a> 
				<ul class="submenu"> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/%EC%86%8D%EA%B9%8A%EC%9D%80%20%EC%9E%90%EB%B0%94%EC%8A%A4%ED%81%AC%EB%A6%BD%ED%8A%B8%20%EA%B0%95%EC%A2%8C" class="submenuLink longLink">menu2-1</a></li> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/%EB%B0%91%EB%B0%94%EB%8B%A5%EB%B6%80%ED%84%B0%20%ED%99%88%ED%8E%98%EC%9D%B4%EC%A7%80%20%EB%A7%8C%EB%93%A4%EA%B8%B0" class="submenuLink longLink">menu2-2</a></li> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/Android%28%EC%95%88%EB%93%9C%EB%A1%9C%EC%9D%B4%EB%93%9C%29%20%EC%95%B1%20%EA%B0%9C%EB%B0%9C" class="submenuLink longLink">menu2-3</a></li>
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/Android%28%EC%95%88%EB%93%9C%EB%A1%9C%EC%9D%B4%EB%93%9C%29%20%EC%95%B1%20%EA%B0%9C%EB%B0%9C" class="submenuLink longLink">menu2-4</a></li>
				</ul>
			</li>		
			<li class="topMenuLi"> 
				<a class="menuLink" href="http://unikys.tistory.com/category/Programming%20Lecture" style="font-size: 1.25rem;">menu3</a> 
				<ul class="submenu"> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/%EC%86%8D%EA%B9%8A%EC%9D%80%20%EC%9E%90%EB%B0%94%EC%8A%A4%ED%81%AC%EB%A6%BD%ED%8A%B8%20%EA%B0%95%EC%A2%8C" class="submenuLink longLink">menu3-1</a></li> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/%EB%B0%91%EB%B0%94%EB%8B%A5%EB%B6%80%ED%84%B0%20%ED%99%88%ED%8E%98%EC%9D%B4%EC%A7%80%20%EB%A7%8C%EB%93%A4%EA%B8%B0" class="submenuLink longLink">menu3-2</a></li> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/Android%28%EC%95%88%EB%93%9C%EB%A1%9C%EC%9D%B4%EB%93%9C%29%20%EC%95%B1%20%EA%B0%9C%EB%B0%9C" class="submenuLink longLink">menu3-3</a></li>
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/Android%28%EC%95%88%EB%93%9C%EB%A1%9C%EC%9D%B4%EB%93%9C%29%20%EC%95%B1%20%EA%B0%9C%EB%B0%9C" class="submenuLink longLink">menu3-4</a></li>
				</ul>
			</li>	
						<li class="topMenuLi"> 
				<a class="menuLink" href="http://unikys.tistory.com/category/Programming%20Lecture" style="font-size: 1.25rem;">menu4</a> 
				<ul class="submenu"> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/%EC%86%8D%EA%B9%8A%EC%9D%80%20%EC%9E%90%EB%B0%94%EC%8A%A4%ED%81%AC%EB%A6%BD%ED%8A%B8%20%EA%B0%95%EC%A2%8C" class="submenuLink longLink">menu4-1</a></li> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/%EB%B0%91%EB%B0%94%EB%8B%A5%EB%B6%80%ED%84%B0%20%ED%99%88%ED%8E%98%EC%9D%B4%EC%A7%80%20%EB%A7%8C%EB%93%A4%EA%B8%B0" class="submenuLink longLink">menu4-2</a></li> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/Android%28%EC%95%88%EB%93%9C%EB%A1%9C%EC%9D%B4%EB%93%9C%29%20%EC%95%B1%20%EA%B0%9C%EB%B0%9C" class="submenuLink longLink">menu4-3</a></li>
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/Android%28%EC%95%88%EB%93%9C%EB%A1%9C%EC%9D%B4%EB%93%9C%29%20%EC%95%B1%20%EA%B0%9C%EB%B0%9C" class="submenuLink longLink">menu4-4</a></li>
				</ul>
			</li>	
						<li class="topMenuLi"> 
				<a class="menuLink" href="http://unikys.tistory.com/category/Programming%20Lecture" style="font-size: 1.25rem;">menu5</a> 
				<ul class="submenu"> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/%EC%86%8D%EA%B9%8A%EC%9D%80%20%EC%9E%90%EB%B0%94%EC%8A%A4%ED%81%AC%EB%A6%BD%ED%8A%B8%20%EA%B0%95%EC%A2%8C" class="submenuLink longLink">속깊은 자바스크립트 강좌</a></li> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/%EB%B0%91%EB%B0%94%EB%8B%A5%EB%B6%80%ED%84%B0%20%ED%99%88%ED%8E%98%EC%9D%B4%EC%A7%80%20%EB%A7%8C%EB%93%A4%EA%B8%B0" class="submenuLink longLink">밑바닥부터 홈페이지 만들기</a></li> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/Android%28%EC%95%88%EB%93%9C%EB%A1%9C%EC%9D%B4%EB%93%9C%29%20%EC%95%B1%20%EA%B0%9C%EB%B0%9C" class="submenuLink longLink">안드로이드 앱 개발</a></li>
				</ul>
			</li>	
			
			<a></a>
            <!-- Navbar Search-->
          <!--  <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
                <div class="input-group">
                    <input class="form-control" type="text" placeholder="Search for..." aria-label="Search for..." aria-describedby="btnNavbarSearch" />
                    <button class="btn btn-primary" id="btnNavbarSearch" type="button"><i class="fas fa-search"></i></button>
                </div>
            </form> -->
            <!-- Navbar-->
            <!-- <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                 <li class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                     <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                         <li><a class="dropdown-item" href="#!">Settings</a></li>
						 <li><a class="dropdown-item" href="#!">Activity Log</a></li>
                         <li><hr class="dropdown-divider" /></li>
                         <li><a class="dropdown-item" href="#!">Logout</a></li>
                     </ul>
                 </li>
             </ul> -->
         </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav" style="display: none;">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Core</div>
                            <a class="nav-link" href="index.html">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Dashboard
                            </a>
                            <div class="sb-sidenav-menu-heading">Interface</div>
                            <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                                <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                                Layouts
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="layout-static.html">Static Navigation</a>
                                    <a class="nav-link" href="layout-sidenav-light.html">Light Sidenav</a>
                                </nav>
                            </div>
                            <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
                                <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                                Pages
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-bs-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                                    <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth">
                                        Authentication
                                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                    </a>
                                    <div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordionPages">
                                        <nav class="sb-sidenav-menu-nested nav">
                                            <a class="nav-link" href="login.html">Login</a>
                                            <a class="nav-link" href="register.html">Register</a>
                                            <a class="nav-link" href="password.html">Forgot Password</a>
                                        </nav>
                                    </div>
                                    <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#pagesCollapseError" aria-expanded="false" aria-controls="pagesCollapseError">
                                        Error
                                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                    </a>
                                    <div class="collapse" id="pagesCollapseError" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordionPages">
                                        <nav class="sb-sidenav-menu-nested nav">
                                            <a class="nav-link" href="401.html">401 Page</a>
                                            <a class="nav-link" href="404.html">404 Page</a>
                                            <a class="nav-link" href="500.html">500 Page</a>
                                        </nav>
                                    </div>
                                </nav>
                            </div>
                            <div class="sb-sidenav-menu-heading">Addons</div>
                            <a class="nav-link" href="charts.html">
                                <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                                Charts
                            </a>
                            <a class="nav-link" href="tables.html">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Tables
                            </a>
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        Start Bootstrap
                    </div>
                </nav>
            </div> 
            <div id="layoutSidenav_content" style="padding-left: 0px;">
				<!-- padding-right: 300px; padding-left: 1.5rem !important; -->
                <main>
                    <div class="container-fluid" style="padding-top: 30px;">
						<div class="card mb-4" style="height: 500px; width: 85%;">
							<div class="card-body">
								<form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0" style="top: 250px; position: relative; left: 410px; width: 800px;">
									<div class="input-group">
										<select style="width: 170px; margin-right: 10px; text-align-last: center;">
											<option>ê²ìë¶ì¼</option>
										</select>
										<input class="form-control" style="width: 400px;" type="text" placeholder="ê²ìì´ë¥¼ ìë ¥íì¸ì." aria-label="Search for..." aria-describedby="btnNavbarSearch" />
										<button class="btn btn-primary" style="margin-left: 10px; border-radius: 0;" id="btnNavbarSearch" type="button"><i class="fas fa-search"></i></button>
									</div>
								</form>
							</div>
						</div>
						
						<div class="row" style="height: 350px; width: 85%;">
							<div class="col-xl-3 col-md-6">
								<div style="height: 60%;">
									<img src="assets/img/img.jpg" style="width: 100%; height: 100%;">
								</div>
								<div style="height: 35%; border: 1px solid rgba(0, 0, 0, 0.3);">
									<h4>Patti announces flagship store poening</h4>
									<h6>Adele Vance&nbsp;&nbsp;&nbsp;&nbsp;<span>2021.06.19</span></h6>
								</div>
							</div>
							<div class="col-xl-3 col-md-6">
								<div style="height: 60%;">
									<img src="assets/img/img.jpg" style="width: 100%; height: 100%;">
								</div>
								<div style="height: 35%; border: 1px solid rgba(0, 0, 0, 0.3);">
									<h4>Patti announces flagship store poening</h4>
									<h6>Adele Vance&nbsp;&nbsp;&nbsp;&nbsp;<span>2021.06.19</span></h6>
								</div>
							</div>
							<div class="col-xl-3 col-md-6">
								<div style="height: 60%;">
									<img src="assets/img/img.jpg" style="width: 100%; height: 100%;">
								</div>
								<div style="height: 35%; border: 1px solid rgba(0, 0, 0, 0.3);">
									<h4>Patti announces flagship store poening</h4>
									<h6>Adele Vance&nbsp;&nbsp;&nbsp;&nbsp;<span>2021.06.19</span></h6>
								</div>
							</div>
							<div class="col-xl-3 col-md-6">
								<div style="border: 1px solid rgba(0, 0, 0, 0.3); height: 95%">
									<div style="height: 33%; padding-top: 30px; margin: 0 20px; border-bottom: 1px solid;">
										<h4 style="text-align: center;">Why simplicity matters</h4>
									</div>
									<div style="height: 33%; padding-top: 30px; margin: 0 20px; border-bottom: 1px solid;">
										<h4 style="text-align: center;">Why simplicity matters</h4>
									</div>
									<div style="height: 33%; padding-top: 30px; margin: 0 20px;">
										<h4 style="text-align: center;">Why simplicity matters</h4>
									</div>
								</div>
							</div>
						</div>
						
						<div class="card mb-4" style="height: 130px; width: 85%;">
							<div>
							
							</div>
						</div>
						
						<div class="row" style="width: 85%;">
							<div class="col-xl-3 col-md-6" style="width: 33.3%">
								<div style="height: 40%;">
									<img src="assets/img/img.jpg" style="width: 100%; height: 100%;">
								</div>
								<div style="height: 55%;">
									<h5 style="margin: 26px 0px; color: cyan;">BREATHTAKING VIDEOS & PHOTOS</h5>
									<p>KOYOCO MARK8 is the first drone to offer an 8K HDR camera that tills vertically at and angle of 180, combinde with an up 2.8X lossless zoom and lolly zoom dffect.</p>
									<p style="font-weight: bold;">-</p>
									<p style="font-weight: bolder">2.8X FULL HD ZOOM</p>
									<p>Reveal your inner fitmmaker</p>
									<div class="wrap">
										<a href="#" class="button2">LEARN MORE</a>
									</div>
								</div>
							</div>
							<div class="col-xl-3 col-md-6" style="width: 33.3%">
								<div style="height: 40%;">
									<img src="assets/img/img.jpg" style="width: 100%; height: 100%;">
								</div>
								<div style="height: 55%;">
									<h5 style="margin: 26px 0px; color: cyan;">BREATHTAKING VIDEOS & PHOTOS</h5>
									<p>KOYOCO MARK8 is the first drone to offer an 8K HDR camera that tills vertically at and angle of 180, combinde with an up 2.8X lossless zoom and lolly zoom dffect.</p>
									<p style="font-weight: bold;">-</p>
									<p style="font-weight: bolder">2.8X FULL HD ZOOM</p>
									<p>Reveal your inner fitmmaker</p>
									<div class="wrap">
										<a href="#" class="button2">LEARN MORE</a>
									</div>
								</div>
							</div>
							<div class="col-xl-3 col-md-6" style="width: 33.3%">
								<div style="height: 40%;">
									<img src="assets/img/img.jpg" style="width: 100%; height: 100%;">
								</div>
								<div style="height: 55%;">
									<h5 style="margin: 26px 0px; color: cyan;">BREATHTAKING VIDEOS & PHOTOS</h5>
									<p>KOYOCO MARK8 is the first drone to offer an 8K HDR camera that tills vertically at and angle of 180, combinde with an up 2.8X lossless zoom and lolly zoom dffect.</p>
									<p style="font-weight: bold;">-</p>
									<p style="font-weight: bolder">2.8X FULL HD ZOOM</p>
									<p>Reveal your inner fitmmaker</p>
									<div class="wrap">
										<a href="#" class="button2">LEARN MORE</a>
									</div>
								</div>
							</div>
						</div>
						
						<div class="row" style="width: 85%;">
							<div class="col-xl-3 col-md-6" style="width: 60%">
								<img src="assets/img/img.jpg" style="width: 100%; height: 100%;">
							</div>
							<div class="col-xl-3 col-md-6" style="width: 30%; border: 1px solid rgba(0, 0, 0, 0.3); margin-left: 60px;">
								
							</div>
						</div>
						
						
						<div style="width: 14%; float: right; top: -1460px; position: relative; height: 1460px; background-color: azure;">
							<div style="padding-top: 30px; padding-left: 20px;">
								<h5 style="font-weight: bold; display: inline-block;">News</h5>
								<span style="margin-left: 110px; display: inline-block; font-size: 14px;">See all</span>
							</div>
							<div style="border-bottom: 1px solid rgba(0, 0, 0, 0.3);">
								<div>
									<img>
								</div>
								<div>
									<span style="font-weight: bold;">The importance of branding at KOYOCO</span>
									<span>At the heart of branging at KOYOCO</span>
									<span>Adele Vance &nbsp;&nbsp;&nbsp;&nbsp; 2021.06.19<span>
								</div>
							</div>
						</div>
						
						
                    </div>
					
                </main>
                <!--<footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2021</div>
                            <div>
                                <a href="#">Privacy Policy  </a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>-->
				
				<div>
					<div style="background-color : black">
						
					</div>
				</div>
				
        </div>
		
		
		
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="../../../resources/js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="../../../resources/assets/demo/chart-area-demo.js"></script>
        <script src="../../../resources/assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="../../../resources/js/datatables-simple-demo.js"></script>
    </body>
    
        <script>
    function movePage() {
    	location.href = "/Admin/movePage";
    }
    </script>
</html>
