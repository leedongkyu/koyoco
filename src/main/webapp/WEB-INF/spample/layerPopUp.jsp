<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="../resources/css/admin.css">
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
<script type="text/javascript" src="../resources/js/publisher.js"></script> <!-- Layout 구성을 위한 퍼블리싱 스크립트 개발 적용 불 필요 -->
<script type="text/javascript" src="../resources/js/commonui.js"></script>
<style type="text/css">
     /* 별점 */
     .cmm-rating{display:inline-block; transform: rotate(180deg);}
     .cmm-rating input{display:none;}
     .cmm-rating label{display:inline-block;margin-right:5px;font-size:24px;color:#ccc;transform: rotate(-180deg);}
     .cmm-rating :checked ~ label {color:#f90; }
     .cmm-rating label:hover,
     .cmm-rating label:hover ~ label {color:#fc0;}
</style>
</head>
<body>
<div id="view-wrap" aria-hidden="true"> <!-- view-wrap S -->
<div id="content-wrap"> <!-- content-wrap S -->
    <div id="content">
        <!--
            // data-layer-target에 호출할 레이어팝업의 아이디 입력 시 Show/Hide
        -->
        <button type="button" class="btn-text" data-layer-target="BOAML13001"><span>상담일지 팝업열기</span></button>
    </div>
</div> <!-- content-wrap E -->
</div> <!-- view-wrap E -->


<!-- 레이어 팝업 S -->
<!--
    // active 추가 시 레이어팝업 Show
    // 레이어팝업 Show 일경우 레이어팝업은 aria-hidden="false", #view-wrap은 aria-hidden="true"가 되도록 스크립트 작성 함.
-->
<div class="cmm-layer half active" tabindex="0" aria-hidden="false" id="BOAML13001">
    <div class="inner">
        <div class="header">
            <h1>상담일지</h1>
            <button type="button" class="btn-icon close"><span>레이업팝업 닫기</span></button>
        </div>
        <div class="content">
            <div data-form-col="2" data-form-label="inline" class="cmm-write-box">
                <div class="cmm-form">
                    <span class="label">국내기업명</span>
                    <input type="text" title="국내기업명">
                </div>
                <div class="cmm-form">
                    <span class="label">상담자</span>
                    <input type="text" title="상담자">
                </div>
                <div class="cmm-form">
                    <span class="label">상담품목</span>
                    <input type="text" title="상담품목">
                </div>
                <div class="cmm-form">
                    <span class="label">통역원</span>
                    <input type="text" title="통역원">
                </div>
                <div class="cmm-form">
                    <span class="label">바이어회사명</span>
                    <input type="text" title="바이어회사명">
                </div>
                <div class="cmm-form">
                    <span class="label">바이어명</span>
                    <input type="text" title="바이어명">
                </div>
                <div class="cmm-form">
                    <span class="label">거래성사 가능사</span>
                    <span class="cmm-checkbox">
                        <input type="checkbox" id="BOAML13001-01-01" checked="checked">
                        <label for="BOAML13001-01-01">상</label>
                    </span>
                    <span class="cmm-checkbox">
                        <input type="checkbox" id="BOAML13001-01-02">
                        <label for="BOAML13001-01-02">중</label>
                    </span>
                    <span class="cmm-checkbox">
                        <input type="checkbox" id="BOAML13001-01-03">
                        <label for="BOAML13001-01-03">하</label>
                    </span>
                </div>
                <div class="cmm-form">
                    <span class="label">애로 및 요청사항</span>
                    <textarea cols="50" rows="2" title="애로 및 요청사항"></textarea>
                </div>
                <div class="cmm-form">
                    <span class="label">상담액(USD)</span>
                    <input type="text" title="상담액(USD)">
                    <p class="alert">* 상담액 : 3년 이내 성약 가능 예상 금액<br>(Estimated Contract Amount in three years)</p>
                </div>
                <div class="cmm-form">
                    <span class="label">첨부파일</span>
                    <ul class="cmm-thum-list">
                        <li>
                            <p class="name">파일명 파일명 파일</p>
                            <button type="button" class="btn-icon delete"><span>삭제</span></button>
                        </li>
                    </ul>
                </div>
                <div class="cmm-form">
                    <span class="label">계약추진액</span>
                    <input type="text" title="계약추진액">
                    <p class="alert">* 계약추진액 : 1년 이내 성약 가능 예상 금액<br>(Estimated Contract Amount in  a  years)</p>
                </div>
                
                 <div class="cmm-form required">
	                <span class="label">별점주기</span>
	                <p class="info">별점을 입력하세요(1점~5점)</p>
	                <span class="cmm-rating">
	                    <input type="radio" id="cmm-5star-01-01" name="rating">
	                    <label for="cmm-5star-01-01">★</label>
	                    <input type="radio" id="cmm-5star-01-02" name="rating">
	                    <label for="cmm-5star-01-02">★</label>
	                    <input type="radio" id="cmm-5star-01-03" name="rating">
	                    <label for="cmm-5star-01-03">★</label>
	                    <input type="radio" id="cmm-5star-01-04" name="rating">
	                    <label for="cmm-5star-01-04">★</label>
	                    <input type="radio" id="cmm-5star-01-05" name="rating">
	                    <label for="cmm-5star-01-05">★</label>
	                    <!-- ★ -->
	                </span>
            	</div>
                
                <div class="btn-box">
                    <button type="button" class="btn-blue"><span>확인</span></button>
                </div>
            </div>
        </div>
    </div>
    <button type="button" class="dim">부스 임시차단 레이업팝업 닫기</button>
</div>
<!-- [팝업] 상담일지 E -->


<script type="text/javascript">
    $(document).on('click', '.btn-blue', function(){
        layerClose();
    });
</script>
</body>
</html>