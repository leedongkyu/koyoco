<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link rel="shortcut icon" href="../../resources/img/favicon.ico" type="image/x-icon">
<title>Guide > Sample > Loading</title>
<link rel="stylesheet" href="../../resources/css/admin.css">
<script type="text/javascript" src="../../resources/js/publisher.js"></script> <!-- Layout 구성을 위한 퍼블리싱 스크립트 개발 적용 불 필요 -->
<script type="text/javascript" src="../../resources/js/commonui.js"></script>
<title>Insert title here</title>
</head>
<body>
<!--
    // 로딩 함수 호출 시 loadingStart() 호출
    // 모든 레이어팝업 레이어팝업은 aria-hidden="true", #view-wrap은 aria-hidden="true"가 되도록 스크립트 작성 함.

    // 로딩 완료 시 로딩 레이어 팝업 Remove() 후
    // 보여줘야 하는 레이업팝업 또는  #view-wrap은 aria-hidden="false"가 되어야 함.
-->
<div id="view-wrap" aria-hidden="true"> <!-- view-wrap S -->
<div id="content-wrap"> <!-- content-wrap S -->
    <div id="content">
        contentArea
    </div>
</div> <!-- content-wrap E -->
</div> <!-- view-wrap E -->


<script type="text/javascript">
    function loadingStart(){
        var loadingStartHtml = '<div class="cmm-layer loading active" aria-hidden="false">';
            loadingStartHtml += '<div class="inner">';
                loadingStartHtml += '<div class="cmm-loading">';
                    loadingStartHtml += '<div class="loading-img">';
                        loadingStartHtml += '<div>로딩중</div>';
                        loadingStartHtml += '<div></div>';
                        loadingStartHtml += '<div></div>';
                        loadingStartHtml += '<div></div>';
                        loadingStartHtml += '<div></div>';
                    loadingStartHtml += '</div>';
                loadingStartHtml += '</div>';
            loadingStartHtml += '</div>';
            loadingStartHtml += '<div class="dim"></div>';
        loadingStartHtml += '</div>';

        $('body').append(loadingStartHtml).siblings('*[aria-hidden]').attr('aria-hidden','true');
    }

    // loading 호출
    loadingStart();
</script>
</body>
</html>