<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Dashboard - SB Admin</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="css/styles.css" rel="stylesheet" />
		<link href="../../resources/css/styles.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="../../resources/js/commonui.js"></script>
</head>
	
	<style>
		a {
			-webkit-transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
			-moz-transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
			-ms-transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
			-o-transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
			transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
			display: block;
			max-width: 180px;
			text-decoration: none;
			border-radius: 4px;
			padding: 20px 30px;
		}

		a.button2 {
			float: left;
			color: rgba(30, 22, 54, 0.6);
			box-shadow: rgba(30, 22, 54, 0.4) 0 0px 0px 2px inset;
		}

		a.button2:hover {
			color: rgba(255, 255, 255, 0.85);
			box-shadow: rgba(30, 22, 54, 0.7) 0 80px 0px 2px inset;
		}
		
				/* 가로형 드롭다운 메뉴 */
		.topMenu {
            height: 30px;  
            width: 770px;       /* [변경] 하위 메뉴와 동일하게 맞춤 */
            position: relative;
            background-color: #2D2D2D; /* [추가] 늘어난만큼 배경색도 보이도록 수정 */
	    }
	    .topMenu ul {           /* 메인 메뉴 안의 ul을 설정함: 상위메뉴의 ul+하위 메뉴의 ul */
	        list-style-type: none;  
	        margin: 0px;            
	        padding: 0px;           
	    }
	    .topMenu ul li {            /* 메인 메뉴 안에 ul 태그 안에 있는 li 태그의 스타일 적용(상위/하위메뉴 모두) */
	        color: white;               
	        background-color: #2d2d2d;  
	        float: left;                
	        line-height: 30px;          
	        vertical-align: middle;     
	        text-align: center;         
	        -position: relative;         
	    }
	    .menuLink, .submenuLink {           /* 상위 메뉴와 하위 메뉴의 a 태그에 공통으로 설정할 스타일 */
	        text-decoration:none;               
	        display: block;                     
	        width: 150px;                       
	        font-size: 13px;                    
	        font-weight: bold;                  
	        font-family: "Trebuchet MS", Dotum; 
	        
	    }
	    .menuLink {     /* 상위 메뉴의 글씨색을 흰색으로 설정 */
	        color: white;
	    }
	    .topMenuLi:hover .menuLink {    /* 상위 메뉴의 li에 마우스오버 되었을 때 스타일 설정 */
	        color: red;                 
	        background-color: #4d4d4d;
	    }
	    .longLink {     /* 좀 더 긴 메뉴 스타일 설정 */
	        width: 190px;   
	    }
		.submenuLink {          /* 하위 메뉴의 a 태그 스타일 설정 */
	        color: #2d2d2d;             
	        background-color: #DDD;      /* [변경] 배경색 변경 */
	        -border: solid 1px black;    /* [삭제] 테두리 삭제 */
	        -margin-right: -1px;         /* [삭제] 공백 보정 삭제 */
	    }
	    .submenu {              /* 하위 메뉴 스타일 설정 */
	        position: absolute;     
	        height: 0px;            
	        overflow: hidden;       
	        transition: height .2s; 
	        -webkit-transition: height .2s; 
	        -moz-transition: height .2s; 
	        -o-transition: height .2s; 
	        width: 770px;           
	        left: 0;                
	
	
	        background-color: #DDD; /* [추가] 하위 메뉴 전체에 배경색 설정 */
	    }
	    .submenu li {
	        display: inline-block;
	
	
	    }
	    .topMenuLi:hover .submenu { 
	        height: 32px;           
	    }
	    .submenuLink:hover {        
	        color: red;                 
	        background-color: #dddddd;  
	    }
	</style>
	
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="#" style="margin-right: 4rem;"><img alt="" src="../../resources/assets/img/koyoco2.png" style="width: 150px;"> </a>
            <!-- Sidebar Toggle-->
<!--             <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button> -->
			<!-- <a class="navbar-brand ps-3" href="#" onclick="movePage()">menu1</a> -->
			<a class="navbar-brand ps-3 " href="#" onclick="movePage()">menu1</a>
			<a class="navbar-brand ps-3 " href="#" onclick="movePage()">menu2</a>
			<a class="navbar-brand ps-3 " href="#" onclick="movePage()">menu3</a>
			<a class="navbar-brand ps-3 " href="#" onclick="movePage()">menu4</a>
			<a class="navbar-brand ps-3 " href="#" onclick="movePage()">menu5</a>
			<!-- 가로 드롭 메뉴 -->
			<!-- <li class="topMenuLi"> 
				<a class="menuLink" href="http://unikys.tistory.com/category/Programming%20Lecture" style="font-size: 1.25rem;">menu2</a> 
				<ul class="submenu"> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/%EC%86%8D%EA%B9%8A%EC%9D%80%20%EC%9E%90%EB%B0%94%EC%8A%A4%ED%81%AC%EB%A6%BD%ED%8A%B8%20%EA%B0%95%EC%A2%8C" class="submenuLink longLink">menu2-1</a></li> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/%EB%B0%91%EB%B0%94%EB%8B%A5%EB%B6%80%ED%84%B0%20%ED%99%88%ED%8E%98%EC%9D%B4%EC%A7%80%20%EB%A7%8C%EB%93%A4%EA%B8%B0" class="submenuLink longLink">menu2-2</a></li> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/Android%28%EC%95%88%EB%93%9C%EB%A1%9C%EC%9D%B4%EB%93%9C%29%20%EC%95%B1%20%EA%B0%9C%EB%B0%9C" class="submenuLink longLink">menu2-3</a></li>
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/Android%28%EC%95%88%EB%93%9C%EB%A1%9C%EC%9D%B4%EB%93%9C%29%20%EC%95%B1%20%EA%B0%9C%EB%B0%9C" class="submenuLink longLink">menu2-4</a></li>
				</ul>
			</li>		
			<li class="topMenuLi"> 
				<a class="menuLink" href="http://unikys.tistory.com/category/Programming%20Lecture" style="font-size: 1.25rem;">menu3</a> 
				<ul class="submenu"> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/%EC%86%8D%EA%B9%8A%EC%9D%80%20%EC%9E%90%EB%B0%94%EC%8A%A4%ED%81%AC%EB%A6%BD%ED%8A%B8%20%EA%B0%95%EC%A2%8C" class="submenuLink longLink">menu3-1</a></li> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/%EB%B0%91%EB%B0%94%EB%8B%A5%EB%B6%80%ED%84%B0%20%ED%99%88%ED%8E%98%EC%9D%B4%EC%A7%80%20%EB%A7%8C%EB%93%A4%EA%B8%B0" class="submenuLink longLink">menu3-2</a></li> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/Android%28%EC%95%88%EB%93%9C%EB%A1%9C%EC%9D%B4%EB%93%9C%29%20%EC%95%B1%20%EA%B0%9C%EB%B0%9C" class="submenuLink longLink">menu3-3</a></li>
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/Android%28%EC%95%88%EB%93%9C%EB%A1%9C%EC%9D%B4%EB%93%9C%29%20%EC%95%B1%20%EA%B0%9C%EB%B0%9C" class="submenuLink longLink">menu3-4</a></li>
				</ul>
			</li>	
						<li class="topMenuLi"> 
				<a class="menuLink" href="http://unikys.tistory.com/category/Programming%20Lecture" style="font-size: 1.25rem;">menu4</a> 
				<ul class="submenu"> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/%EC%86%8D%EA%B9%8A%EC%9D%80%20%EC%9E%90%EB%B0%94%EC%8A%A4%ED%81%AC%EB%A6%BD%ED%8A%B8%20%EA%B0%95%EC%A2%8C" class="submenuLink longLink">menu4-1</a></li> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/%EB%B0%91%EB%B0%94%EB%8B%A5%EB%B6%80%ED%84%B0%20%ED%99%88%ED%8E%98%EC%9D%B4%EC%A7%80%20%EB%A7%8C%EB%93%A4%EA%B8%B0" class="submenuLink longLink">menu4-2</a></li> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/Android%28%EC%95%88%EB%93%9C%EB%A1%9C%EC%9D%B4%EB%93%9C%29%20%EC%95%B1%20%EA%B0%9C%EB%B0%9C" class="submenuLink longLink">menu4-3</a></li>
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/Android%28%EC%95%88%EB%93%9C%EB%A1%9C%EC%9D%B4%EB%93%9C%29%20%EC%95%B1%20%EA%B0%9C%EB%B0%9C" class="submenuLink longLink">menu4-4</a></li>
				</ul>
			</li>	
						<li class="topMenuLi"> 
				<a class="menuLink" href="http://unikys.tistory.com/category/Programming%20Lecture" style="font-size: 1.25rem;">menu5</a> 
				<ul class="submenu"> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/%EC%86%8D%EA%B9%8A%EC%9D%80%20%EC%9E%90%EB%B0%94%EC%8A%A4%ED%81%AC%EB%A6%BD%ED%8A%B8%20%EA%B0%95%EC%A2%8C" class="submenuLink longLink">속깊은 자바스크립트 강좌</a></li> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/%EB%B0%91%EB%B0%94%EB%8B%A5%EB%B6%80%ED%84%B0%20%ED%99%88%ED%8E%98%EC%9D%B4%EC%A7%80%20%EB%A7%8C%EB%93%A4%EA%B8%B0" class="submenuLink longLink">밑바닥부터 홈페이지 만들기</a></li> 
					<li><a href="http://unikys.tistory.com/category/Programming%20Lecture/Android%28%EC%95%88%EB%93%9C%EB%A1%9C%EC%9D%B4%EB%93%9C%29%20%EC%95%B1%20%EA%B0%9C%EB%B0%9C" class="submenuLink longLink">안드로이드 앱 개발</a></li>
				</ul>
			</li> -->	
			<a></a>
			
            <!-- Navbar Search-->
          <!--  <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
                <div class="input-group">
                    <input class="form-control" type="text" placeholder="Search for..." aria-label="Search for..." aria-describedby="btnNavbarSearch" />
                    <button class="btn btn-primary" id="btnNavbarSearch" type="button"><i class="fas fa-search"></i></button>
                </div>
            </form> -->
            <!-- Navbar-->
           <!-- <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                 <li class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                     <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                         <li><a class="dropdown-item" href="#!">Settings</a></li>
						 <li><a class="dropdown-item" href="#!">Activity Log</a></li>
                         <li><hr class="dropdown-divider" /></li>
                         <li><a class="dropdown-item" href="#!">Logout</a></li>
                     </ul>
                 </li>
             </ul> -->
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav" style="display: none;">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Core</div>
                            <a class="nav-link" href="index.html">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Dashboard
                            </a>
                            <div class="sb-sidenav-menu-heading">Interface</div>
                            <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                                <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                                Layouts
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="layout-static.html">Static Navigation</a>
                                    <a class="nav-link" href="layout-sidenav-light.html">Light Sidenav</a>
                                </nav>
                            </div>
                            <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
                                <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                                Pages
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-bs-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                                    <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth">
                                        Authentication
                                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                    </a>
                                    <div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordionPages">
                                        <nav class="sb-sidenav-menu-nested nav">
                                            <a class="nav-link" href="login.html">Login</a>
                                            <a class="nav-link" href="register.html">Register</a>
                                            <a class="nav-link" href="password.html">Forgot Password</a>
                                        </nav>
                                    </div>
                                    <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#pagesCollapseError" aria-expanded="false" aria-controls="pagesCollapseError">
                                        Error
                                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                    </a>
                                    <div class="collapse" id="pagesCollapseError" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordionPages">
                                        <nav class="sb-sidenav-menu-nested nav">
                                            <a class="nav-link" href="401.html">401 Page</a>
                                            <a class="nav-link" href="404.html">404 Page</a>
                                            <a class="nav-link" href="500.html">500 Page</a>
                                        </nav>
                                    </div>
                                </nav>
                            </div>
                            <div class="sb-sidenav-menu-heading">Addons</div>
                            <a class="nav-link" href="charts.html">
                                <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                                Charts
                            </a>
                            <a class="nav-link" href="tables.html">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Tables
                            </a>
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        Start Bootstrap
                    </div>
                </nav>
            </div> 
            <div id="layoutSidenav_content" style="padding-left: 0px;">
				<!-- padding-right: 300px; padding-left: 1.5rem !important; -->
                <main>
                    <div class="container-fluid" style="padding-top: 30px;">
						<div class="card mb-4" style="height: 500px; width: 84%;">
							<div class="card-body" style="background-image: url('../../resources/assets/img/29313_16006_3127.jpg'); background-size: 100%">
								<form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0" style="top: 250px; position: relative; left: 410px; width: 800px;">
									<div class="input-group">
										<select style="width: 170px; margin-right: 10px; text-align-last: center;">
											<option>검색분야</option>
										</select>
										<input class="form-control" style="width: 400px;" type="text" placeholder="검색어를 입력하세요." aria-label="Search for..." aria-describedby="btnNavbarSearch" />
										<button class="btn btn-primary" style="margin-left: 10px; border-radius: 0;" id="btnNavbarSearch" type="button"><i class="fas fa-search"></i></button>
									</div>
								</form>
							</div>
						</div>
						
						<div class="row" style="height: 350px; width: 85%;">
							<div class="col-xl-3 col-md-6">
								<div style="height: 60%;">
									<img src="../../resources/assets/img/img.jpg" style="width: 100%; height: 100%;">
								</div>
								<div style="height: 35%; border: 1px solid rgba(0, 0, 0, 0.3);">
									<button data-layer-target="BOACL13101" style="background-color:transparent; border: 0; outline: 0; text-align: left;">
										<h4>Patti announces flagship store poening</h4>
										<h6>Adele Vance&nbsp;&nbsp;&nbsp;&nbsp;<span>2021.06.19</span></h6>
									</button>
								</div>
							</div>
							<div class="col-xl-3 col-md-6">
								<div style="height: 60%;">
									<img src="../../resources/assets/img/img_1.jpg" style="width: 100%; height: 100%;">
								</div>
								<div style="height: 35%; border: 1px solid rgba(0, 0, 0, 0.3);">
									<button data-layer-target="BOACL13101" style="background-color:transparent; border: 0; outline: 0; text-align: left;">
										<h4>Patti announces flagship store poening</h4>
										<h6>Adele Vance&nbsp;&nbsp;&nbsp;&nbsp;<span>2021.06.19</span></h6>
									</button>
								</div>
							</div>
							<div class="col-xl-3 col-md-6">
								<div style="height: 60%;">
									<img src="../../resources/assets/img/xary.jpg" style="width: 100%; height: 100%;">
								</div>
								<div style="height: 35%; border: 1px solid rgba(0, 0, 0, 0.3);">
									<button data-layer-target="BOACL13101" style="background-color:transparent; border: 0; outline: 0; text-align: left;">
										<h4>Patti announces flagship store poening</h4>
										<h6>Adele Vance&nbsp;&nbsp;&nbsp;&nbsp;<span>2021.06.19</span></h6>
									</button>
								</div>
							</div>
							<div class="col-xl-3 col-md-6">
								<div style="border: 1px solid rgba(0, 0, 0, 0.3); height: 95%">
									<div style="height: 117px; padding-top: 15px; margin: 0 20px; border-bottom: 1px solid; overflow: hidden;">
										<span>Purpose: The objectives of this study were to understand and compare perception and experience between clinical staffs (nurses and pharmacists) and Quality Improvement managers. Method: A qualitative study was conducted with 14 clinical staffs and QI managers who are working at tertiary hospitals in Korea. Interviews were recorded and transcribed for systematic analyses of qualitative data. Results: Most critically, while QI managers acknowledged that establishment of the patient safety culture and reduction of medical errors are urgent tasks for QI effort, clinical staffs don`t seem to share such perceptions, all participants agree that staff shortage and no compliance to safety procedures were major reasons for medical error occurrences. Many suggested that an organizational culture where errors were perceived as a systematic problems rather than individual failures or carelessness should be formed to promote voluntary reporting of medical errors. Conclusion: A more systematic effort and attention at the hospital leadership and public policy level should be promoted to constitute societal consensus on the urgence of promoting patient safety culture and more specific approaches to tackle the patient safety problems.</span>
									</div>
									<div style="height: 117px; padding-top: 15px; margin: 0 20px; border-bottom: 1px solid; overflow: hidden;">
										<span>Objectives: The purpose of this study is to evaluate the influence of depression symptom on the self-rated health status(SRHS), the outpatient health service utilization and quality of life(QOL) also the relationship depression symptom with socio-demographic and health related factors. Methods: We selected 9,550 participants without chronic diseases from a total of 18,104 in the '2009 community health survey in Gyeongnam. They were assessed by using a Korean version of the Center for Epidemiological Studies-Depression Scale(CES-D). Those with CES-D scores of 21 or greater were defined as having probable depression. Results: A probable depression were associated in bivariate analysis with gender, age, educational status, monthly household income, marital status, current smoking status, drinking habit, physical activities and body mass index. After adjustment for covariates, probable depression groups predicted a lower status in SRHS. Likewise probable depression groups predicted a higher utilization in outpatient health service. Also probable depression groups predicted a lower score in QOL. Conclusions: Probable depression influence SRHS, outpatient health service utilization and QOL even after adjusting for the socio-demographic, health related factors and chronic medical illness. Programs for prevention and management of depression will be helpful to promote health and QOL.</span>
									</div>
									<div style="height: 117px; padding-top: 15px; margin: 0 20px; overflow: hidden;">
										<span>의료인류학에서 연구하는 의료는 무엇인가? 의료인류학은 의료를 어떻게 연구하는가? 의료인류학에서 의료와 인류학의 관계는 무엇인가? 이러한 본질적 질문을 위해 본 논문은 의료인류학의 연구 분야를 의료의 인류학과 의료 관련 인류학으로 나누어 논의해보고자 한다. 이러한 구분은 잠정적 구분이며, 분류를 위해서라기 보다는 그 구분을 넘어서는 의료인류학의 가능성을 타진해 보기 위한 노력이다. 그동안의 의료인류학에서 의료 관련 인류학이 주를 이루었던 것은 인류학의 형이상학적 전제와 관련이 있으며 자연과 문화의 분리가 의료 내부와 의료 주변의 인류학으로 의료인류학을 나누는 결과를 낳았다. 이러한 구분을 넘어서는 의료인류학을 위해서는 실재에 대한 논의가 필요하다. 실재에 대한 논의는 데스콜라의 “확인(identification)”과 몰의 “연행(enactment)”의 개념의 접점을 통해 논의 가능하다. 생의학과 한의학에 대한 현지조사 자료들은, 실재는 그 의학의 내용뿐만 아니라 그 실재에 연결된 사회적 정치적 실천의 논의 가능성을 열어 놓는다는 것을 보여준다. 코비드-19 팬데믹 이후 더 많은 의료인류학적 논의가 요구되는 상황에서 의료 내부와 외부를 관통하는, 실재와 실천을 연결하는 연구와 토론의 기여 가능성은 열려있다.</span>
									</div>
								</div>
							</div>
						</div>
						
						<div class="card mb-4" style="height: 130px; width: 84%; margin: 20px 0px;">
							<div>
							
							</div>
						</div>
						
						<div class="row" style="width: 85%;">
							<div class="col-xl-3 col-md-6" style="width: 33.3%">
								<div style="height: 40%;">
									<img src="../../resources/assets/img/MedicalEquipment.jpg" style="width: 100%; height: 100%;">
								</div>
								<div style="height: 55%;">
									<h5 style="margin: 26px 0px; color: cyan;">BREATHTAKING VIDEOS & PHOTOS</h5>
									<p>KOYOCO MARK8 is the first drone to offer an 8K HDR camera that tills vertically at and angle of 180, combinde with an up 2.8X lossless zoom and lolly zoom dffect.</p>
									<p style="font-weight: bold;">-</p>
									<p style="font-weight: bolder">2.8X FULL HD ZOOM</p>
									<p>Reveal your inner fitmmaker</p>
									<div class="wrap">
										<a href="#" class="button2">LEARN MORE</a>
									</div>
								</div>
							</div>
							<div class="col-xl-3 col-md-6" style="width: 33.3%">
								<div style="height: 40%;">
									<img src="../../resources/assets/img/ct.jpg" style="width: 100%; height: 100%;">
								</div>
								<div style="height: 55%;">
									<h5 style="margin: 26px 0px; color: cyan;">BREATHTAKING VIDEOS & PHOTOS</h5>
									<p>KOYOCO MARK8 is the first drone to offer an 8K HDR camera that tills vertically at and angle of 180, combinde with an up 2.8X lossless zoom and lolly zoom dffect.</p>
									<p style="font-weight: bold;">-</p>
									<p style="font-weight: bolder">2.8X FULL HD ZOOM</p>
									<p>Reveal your inner fitmmaker</p>
									<div class="wrap">
										<a href="#" class="button2">LEARN MORE</a>
									</div>
								</div>
							</div>
							<div class="col-xl-3 col-md-6" style="width: 33.3%">
								<div style="height: 40%;">
									<img src="../../resources/assets/img/istockphoto-487827487-1024x1024.jpg" style="width: 100%; height: 100%;">
								</div>
								<div style="height: 55%;">
									<h5 style="margin: 26px 0px; color: cyan;">BREATHTAKING VIDEOS & PHOTOS</h5>
									<p>KOYOCO MARK8 is the first drone to offer an 8K HDR camera that tills vertically at and angle of 180, combinde with an up 2.8X lossless zoom and lolly zoom dffect.</p>
									<p style="font-weight: bold;">-</p>
									<p style="font-weight: bolder">2.8X FULL HD ZOOM</p>
									<p>Reveal your inner fitmmaker</p>
									<div class="wrap">
										<a href="#" class="button2">LEARN MORE</a>
									</div>
								</div>
							</div>
						</div>
						
						<div class="row" style="width: 85%;">
							<div class="col-xl-3 col-md-6" style="width: 60%">
								<img src="assets/img/img.jpg" style="width: 100%; height: 100%;">
							</div>
							<div class="col-xl-3 col-md-6" style="width: 30%; border: 1px solid rgba(0, 0, 0, 0.3); margin-left: 60px;">
								
							</div>
						</div>
						
						
						<div style="width: 14%; float: right; top: -1910px; position: relative; height: 1460px; background-color: azure;">
							<div style="padding-top: 30px; padding-left: 20px;">
								<h5 style="font-weight: bold; display: inline-block;">News</h5>
								<span style="margin-left: 110px; display: inline-block; font-size: 14px;">See all</span>
							</div>
							<div style="border-bottom: 1px solid rgba(0, 0, 0, 0.3);">
								<div>
									<img>
								</div>
								<div>
									<span style="font-weight: bold;">The importance of branding at KOYOCO</span>
									<span>At the heart of branging at KOYOCO</span>
									<span>Adele Vance &nbsp;&nbsp;&nbsp;&nbsp; 2021.06.19<span>
								</div>
							</div>
							<div style="border-bottom: 1px solid rgba(0, 0, 0, 0.3);">
								<div>
									<img>
								</div>
								<div>
									<span style="font-weight: bold;">간호학생의 생명의료윤리의식 영향 요인무료</span>
									<br />
									<span>Affecting Factors of the Awareness of Biomedical Ethics in Nursing Students 본 연구에서 간호학생의 생명의료윤리의식 정도는 중간정도로 나타났고, 죽음에 대한 인식은 중상...</span>
									<br /><br />
									<span>2017 정유리 외 1 명<span>
								</div>
							</div>
							<div style="border-bottom: 1px solid rgba(0, 0, 0, 0.3);">
								<div>
									<img>
								</div>
								<div>
									<span style="font-weight: bold;">간호사의 생명의료윤리에 대한 의식무료</span>
									<br />
									<span>The Perception of Biomedical Ethics in Nurses Purpose: This study was to describe the perception of biomedical ethics in 210 nurses working at a hospital in Busan. Method:...</span>
									<br /><br />
									<span>2009 하주영 외 2 명<span>
								</div>
							</div>
							<div style="border-bottom: 1px solid rgba(0, 0, 0, 0.3);">
								<div>
									<img>
								</div>
								<div>
									<span style="font-weight: bold;">간호대학 1학년 학생을 위한 생명의료윤리 교육 프로그램의 효과</span>
									<br />
									<span>The Effect of Biomedical Ethics Education Program for Nursing Students Freshman Purpose: This study was conducted to exam the effect of a biomedical ethics education...</span>
									<br /><br />
									<span>2016 정계선<span>
								</div>
							</div>
							<div style="border-bottom: 1px solid rgba(0, 0, 0, 0.3);">
								<div>
									<img>
								</div>
								<div>
									<span style="font-weight: bold;">원격의료서비스 수용요인의 구조적 관계 실증연구</span>
									<br />
									<span>목차 원격의료서비스 수용요인의 구조적 관계 실증연구 / 김성수 ; 류시원 1 [요약] 1 I. 서론 2 II. 이론적 배경 4 2.1. 원격의료의 개념 4 2.2. 기술수용모델 5 III. 연구모형 및...</span>
									<br /><br />
									<span>2011 김성수 외 1 명<span>
								</div>
							</div>
						</div>
						
						
                    </div>
					
                </main>
                <!--<footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2021</div>
                            <div>
                                <a href="#">Privacy Policy  </a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>-->
				
				<div>
					<div style="background-color : black">
						
					</div>
				</div>
				
		</div>
		
		
        <div class="cmm-layer half" aria-hidden="false" id="BOACL13101">
			<div class="inner">
				<div class="header">
		            <h1>비디오 플레이</h1>
		            <button type="button" class="btn-icon close"><span>직원 및 기업 회원찾기 레이업팝업 닫기</span></button>
		        </div>
		        <div class="content">
		        	<video width="400" controls>
						<source src="../../resources/assets/img/Next_Level.mp4" type="video/mp4">
					</video>
<!-- 					<video src="../../resources/assets/img/Next_Level.mp4" type="video/mp4" /> -->
		            <div class="sub-content">
		                <div class="cmm-table" id="numGrid"></div>
		                <div class="btn-box">
		                    <button type="button" class="btn-gray"><span>닫기</span></button>
		                </div>
		            </div>
	       	 	</div>
			</div>
			<button type="button" class="dim">직원 및 기업 회원찾기 레이업팝업 닫기</button>
		</div>
		
		
		
		
		
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="../../../resources/js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="../../../resources/assets/demo/chart-area-demo.js"></script>
        <script src="../../../resources/assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="../../../resources/js/datatables-simple-demo.js"></script>
    </body>
        <script>
    function movePage() {
    	location.href = "/Admin/movePage";
    }
    </script>
</html>
