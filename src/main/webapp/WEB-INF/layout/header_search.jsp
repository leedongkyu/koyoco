<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!--
/*************************************************************************
* @ 서비스경로 : Guide > Sample > Layout
* @ 파일명    : 01-layout.html
* @ 작성자    : joar
* @ 작성일    :2022-01-22
************************** 수정이력 ****************************************
* 날짜                    작업자                 변경내용
*_________________________________________________________________________
* 2022-01-22             joar                 최초작성
*************************************************************************/
-->    
<jsp:include page="/WEB-INF/common/commonInclude.jsp"></jsp:include>
<title>KoYoCo</title>
</head>
<body>
<div id="view-wrap" aria-hidden="false" class="layout-guide search-box"> <!-- view-wrap S -->
    <div id="skip-menu">
        <a href="#gnb"><span>메인메뉴 바로가기</span></a>
        <a href="#snb"><span>서브메뉴 바로가기</span></a>
        <a href="#content"><span>컨텐츠 바로가기</span></a>
    </div>
    <div id="header">
        <div class="top-box">
            <a class="logo" href="/board/thesis/page"><span>KoYoCo</span></a>
            <div class="top-flex">
            	<a class="btn-blue" id="write" href="/board/thesis/register/page"><span>문서등록</span></a>
                <a class="mypage" id="mypage"><span>마이페이지</span></a>
<!--                 href="/member/detail/user" -->
            </div>
        </div>
        <div class="mypage-pop" id="no">
            <a id="login" href="/signUp/login"><span>로그인</span></a>
            <a id="signUp" href="/signUp/signUpCheck"><span>회원가입</span></a>
        </div>
        <div class="mypage-pop" id="yes">
            <a id="change" href="/member/detail/user"><span>정보 변경</span></a>
            <a id="list" href="javascript:;"><span>찜 목록</span></a>
            <a id="history" href="javascript:;"><span>구매내역</span></a>
            <a id="mng" href="javascript:;"><span>정산관리</span></a>
            <a id="doc" href="javascript:;"><span>내 문서</span></a>
        </div>
        <form action="/board/thesis/page" style="width: 100%; height: 100%;">
	        <div class="top-search">
	            <p>쉽고, 빠르게 의학에 관련된 문서를 찾아보세요.</p>
	            <div class="cmm-flex">
	                <input type="text" placeholder="검색어를 입력해 주세요." name="title" value="${title}" required="required">
	                <button class="btn-icon search"><span>검색</span></button>
	            </div>
	        </div>
		</form>
    </div>
<script type="text/javascript">
$(document).ready(function(){
	<%
	if(session != null){
	    String userId = (String)session.getAttribute("userId");
	    if(userId != null){
	%>    	
    	$("#write").show();
    	$("#no").remove();
	<%
	    }else{
   	%>
   		$("#write").hide();
   		$("#yes").remove();
   	<%
	    }
	}
	%>	
	$("#mypage").click(function(){
		$('.mypage-pop').toggle();
	});
});
</script>