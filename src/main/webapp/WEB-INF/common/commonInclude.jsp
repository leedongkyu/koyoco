<!DOCTYPE html>
<script type="text/javascript" src="${CONTEXT}/resources/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="${CONTEXT}/resources/js/commonui.js"></script>
<script type="text/javascript" src="${CONTEXT}/resources/js/comutil.js"></script>
<script type="text/javascript" src="${CONTEXT}/resources/js/common/common.util.js"></script>
<script type="text/javascript" src="${CONTEXT}/resources/js/tui/tui-chart.min.js"></script>
<script type="text/javascript" src="${CONTEXT}/resources/js/tui/tui-chart-all.js"></script>
<link href="${CONTEXT}/resources/js/tui/tui-chart.min.css" rel="stylesheet" />
<link href="${CONTEXT}/resources/css/styles.css" rel="stylesheet" />
<link href="${CONTEXT}/resources/css/admin.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>