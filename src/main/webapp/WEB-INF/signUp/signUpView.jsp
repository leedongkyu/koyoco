<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/common/commonInclude.jsp"></jsp:include>
<head>
<meta charset="UTF-8">
<title>회원가입</title>
</head>
<script type="text/javascript" src="../resources/js/signUp/signUpView.js"></script>
<body class="bg-primary">
    <div id="layoutAuthentication">
        <div id="layoutAuthentication_content" style="background:linear-gradient( to bottom, #f9e9db,#5a9cff);" >
            <main>
                <div class="container">
                    <div class="row justify-content-center" style="text-align:center;">
                        <div class="col-lg-7">
                    		<img src="../../resources/assets/img/koyoco2.png" style="width:50%; margin-right: 25px; ">
                            <div class="card shadow-lg border-0 rounded-lg mt-5">
                                <div class="card-header"><h3 class="text-center font-weight-light my-4">회원가입</h3></div>
                                <div class="card-body wrap">
	                                <div class="container">
		                                <form id="form" name="form">
			                                <div class="signUpList">
		                                    	<div class="form-floating mb-3 required">
		                                            <input class="form-control" id="USER_NM" name="USER_NM" type="text" placeholder="이름"/>
		                                            <label class="label" for="name">이름</label>
		                                        </div>
		                                        <div class="form-floating mb-3 required">
		                                            <input class="form-control" id="USER_ID" name="USER_ID" type="text" placeholder="이메일(아이디)"/>
		                                            <label class="label" for="email">이메일(아이디)</label>
		                                        </div>
		                                        <div class="form-floating mb-3 required">
		                                            <input class="form-control" id="USER_PW" name="USER_PW" type="password" placeholder="비밀번호"/>
		                                            <label class="label" for="password">비밀번호</label>
		                                        </div>
		                                        <div class="form-floating mb-3 required">
		                                            <input class="form-control" id="USER_PW" name="USER_PW" type="password" placeholder="비밀번호 확인"/>
		                                            <label class="label" for="passwordChk">비밀번호 확인</label>
		                                        </div>
		                                        <div class="mt-4 mb-0">
		                                            <button type="button" class="btn btn-primary btn-lg btn-block" id="lfn_save" name="lfn_save">회원가입</button>
		                                        </div>
	                                        </div>
	                                    </form>
	                                </div>
                                </div>
                                <div class="card-footer text-center py-3"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>
</body>
</html>