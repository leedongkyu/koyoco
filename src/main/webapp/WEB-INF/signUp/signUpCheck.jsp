<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/common/commonInclude.jsp"></jsp:include>
<head>
<meta charset="UTF-8">
<title>약관동의</title>
</head>
<script>
$(document).ready(function(){
	$("#chkNext").on("click", function(){
		if($("#req").is(":checked") == true){
			$("#form").submit();
		}else{
			alert("이용약관에 동의하셔야 합니다.");
		}
	})	
})

</script>
    <body class="bg-primary">
    
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content" style="background:linear-gradient( to bottom, #f9e9db,#5a9cff);" >
                <main>
                    <div class="container">
                    <form action="/signUp/signUp" id="form" name="form" method="post">
	                    <div class="row justify-content-center" style="text-align:center;">
	                        <div class="col-lg-7">
		                        <img src="../../resources/assets/img/koyoco2.png" style="width:50%; margin-right: 25px; ">
		                        <div class="card shadow-lg border-0 rounded-lg mt-5">
			                        <div class="card-header"><h3 class="text-center font-weight-light my-4">이용약관</h3></div>
				                        <div class="card-body" style="text-align:left;">
										   <p class = "agree">제 1조 Lorem ipsum dolor sit amet<br>
										   이 약관은 (0)0000(전자거래 사업자)이 운영하는 홈페이지(이하 "코요코"이라 한다)에서 제공하는 인터넷 관련 서비스(이하 "서비스"라 한다)를 이용함에 있어 (0)0000와 이용자의 권리·의무 및 책임사항을 규정함을 목적으로 합니다.<br>
										   ※ 「PC통신 등을 이용하는 전자거래에 대해서도 그 성질에 반하지 않는 한 이 약관을 준용합니다」<br>
										   제2조(정의)   
										   ① "코요코" 이란 사업자가 재화 또는 용역을 이용자에게 제공하기 위하여 컴퓨터 등 정보통신설비를 이용하여 재화 또는 용역을 거래할 수 있도록 설정한 가상의 영업장을 말하며, 아울러 코요코을 운영하는 사업자의 의미로도 사용합니다.<br>
										   ② "이용자"란 "코요코"에 접속하여 이 약관에 따라 "코요코"이 제공하는 서비스를 받는 회원 및 비회원을 말합니다.<br>
										   ③ "회원"이라 함은 "코요코"에 개인정보를 제공하여 회원등록을 한 자로서, "코요코"의 정보를 지속적으로 제공받으며, "코요코"이 제공하는 서비스를 계속적으로 이용할 수 있는 자를 말합니다.<br>
										   ④ "비회원"이라 함은 회원에 가입하지 않고 "코요코"이 제공하는 서비스를 이용하는 자를 말합니다.</p>
				                        </div>
				                        <div class="card-footer text-center py-3">
				                            <div class="small">
				                            	<label><input type="checkbox" id="req" name="req"> 동의합니다.</label> 
				                            </div>
				                        </div>
								</div>	  
							</div>
							<div class="container">
								<div class="row justify-content-center" style="text-align:center;">
									<div class="col-lg-7">
										<div class="card shadow-lg border-0 rounded-lg mt-5">
										<button type="button" class="btn btn-primary btn-lg btn-block" id="chkNext">약관에 동의합니다.</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						</form>
					</div>
                </main>
            </div>
        </div>
    </body>
</html>