<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/common/commonInclude.jsp"></jsp:include>
<head>
<meta charset="UTF-8">
<title>로그인</title>
</head>
<script type="text/javascript" src="../resources/js/signUp/login.js"></script>
<body class="bg-primary">
	<div id="layoutAuthentication">
	    <div id="layoutAuthentication_content"  style="background: linear-gradient( to bottom, #f9e9db,#5a9cff);">
	        <main>
	            <div class="container">
	                <div class="row justify-content-center">
	                    <div class="col-lg-5" style="top: 50%;text-align: center;">
	                        <img src="../../resources/assets/img/koyoco2.png" style="width: 50%;margin-right: 25px;margin-top: 50px;">
		                        <div class="card shadow-lg border-0 rounded-lg mt-5" style="top: 25px;">
		                            <div class="card-header"><h3 class="text-center font-weight-light my-4">로그인</h3></div>
		                            <div class="card-body">
		                                <form id="form" name="form">
			                                <div class="loginList">
				                                <div class="form-floating mb-3 required">
			                                        <input class="form-control" id="userId" name="userId" type="text" placeholder="이메일(아이디)"/>
			                                        <label class="label" for="email">이메일(아이디)</label>
			                                    </div>
			                                    <div class="form-floating mb-3 required">
			                                        <input class="form-control" id="userPw" name="userPw" type="password" placeholder="비밀번호" />
			                                        <label class="label" for="email">비밀번호</label>
			                                    </div>
			                                    <div class="d-flex align-items-center justify-content-between mt-4 mb-0">
			                                        <a class="small" href="password.html">비밀번호 찾기</a>
			                                        <button type="button" class="btn btn-primary btn-lg btn-block btn-sm" id="lfn_select" name="lfn_select">로그인</button>
			                                    </div>
			                                </div>
		                                </form>
		                            </div>
		                            <div class="card-footer text-center py-3">
	                                	<div class="small"><a href="signUpCheck">회원가입</a></div>
	                            	</div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </main>
	    </div>
	</div>
</body>
</html>
