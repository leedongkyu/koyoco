<%--
  Created by IntelliJ IDEA.
  User: 코요코3
  Date: 2021-12-18
  Time: 오후 1:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html>
<html lang="ko">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="format-detection" content="telephone=no">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<title>Guide > Sample > Layout</title>
<!-- Layout 구성을 위한 퍼블리싱 스크립트 개발 적용 불 필요 -->
<jsp:include page="/WEB-INF/layout/header.jsp"></jsp:include>
</head>
<script type="text/javascript" src="${CONTEXT}/resources/js/board/thesisRegister.js"></script>

<body>
		<div id="">
			<!-- content-wrap S -->
			<div id="content">
				<form action="" id="form">
					<div class="sub-content">
						<div class="cmm-form required" style="width: 90%;display: -webkit-inline-box; margin-bottom: 20px;">
							<span class="label" style="width: 5%;">TITLE</span>
							<input type="text" id="THESIS_TITLE" name="THESIS_TITLE" title="타이틀">
						</div>
						<div class="cmm-form required" style="width: 90%;display: -webkit-inline-box; margin-bottom: 20px;">
							<span class="label" style="width: 5%;">발행연도</span>
							<input type="text" id="THESIS_YEAR" name="THESIS_YEAR" title="발행연도" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');">
						</div>
						<div class="cmm-form required" style="width: 90%;display: -webkit-inline-box; margin-bottom: 20px;">
							<span class="label" style="width: 5%;">저자</span>
							<input type="text" id="THESIS_AUTHOR" name="THESIS_AUTHOR" title="저자">
						</div>
						<div class="cmm-form required" style="width: 90%;display: -webkit-inline-box; margin-bottom: 20px;">
							<span class="label" style="width: 5%;">가격</span>
							<input type="text" id="THESIS_PRICE" name="THESIS_PRICE" title="가격" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');">
						</div>
						<div class="cmm-form required" style="width: 90%; margin-bottom: 20px;">
							<span class="label" style="width: 5%;">저자소개</span>
							<textarea rows="20" id="AUTHOR_INTRO" name="AUTHOR_INTRO"></textarea>
						</div>
						<div class="cmm-form required" style="width: 90%; margin-bottom: 20px;">
							<span class="label" style="width: 5%;">문서소개</span>
							<textarea rows="20" id="DOCU_INTRO" name="DOCU_INTRO"></textarea>
						</div>
						<div class="cmm-form required" style="width: 90%; margin-bottom: 20px;">
							<span class="label" style="width: 5%;">부가정보</span>
							<textarea rows="20" id="ADD_INFO" name="ADD_INFO"></textarea>
						</div>
						<div class="btn-box" style="text-align: center;">
							<button type="button" class="btn-blue" id="btnSave"><span>등록</span></button>
						</div>
					</div>
				</form>
			</div>
			<!-- content-wrap E -->
			<div class="p2_main_footer">
				<div class="p2_size_content">
					<div class="p2_footer_logo_box">
						<a href="#" class="p2_kep_logo"> <img
							src="../../resources/img/00_oeuhwk/img_logo_gray.png" alt="logo">
						</a>
					</div>
					<div class="p2_footer_content_box">
						<div class="p2_footer_list">
							<a href=""><span>전시회</span></a> <a href=""><span>KEP소개</span></a>
							<a href=""><span>지원센터</span></a>
						</div>
						<div class="p2_footer_content">
							<div class="p2_share_box">
								<button class="p2_share_mail">
									<i></i> <span>뉴스레터 구독</span>
								</button>
								<button class="p2_share_facebook">
									<span>facebook</span>
								</button>
								<button class="p2_share_linkedin">
									<span>linkedin</span>
								</button>
								<button class="p2_share_twitter">
									<span>twitter</span>
								</button>
								<button class="p2_share_instagram">
									<span>instagram</span>
								</button>
							</div>
							<div class="p2_footer_btn_term">
								<button>
									<span>이용약관</span>
								</button>
								<hr>
								<button>
									<span>저작권정책</span>
								</button>
								<hr>
								<button class="p2_active">
									<span>개인정보처리 방침</span>
								</button>
							</div>
							<div class="p2_footer_info_box">
								<div class="p2_footer_address">
									<span>067792) 서울시 서초구 헌릉로 13</span> <span>사업자등록번호
										120-82-00275</span> <span>TEL 1600-7119</span>
								</div>
								<span class="p2_footer_copyright">COPYRIGHT(c)2021 KOTRA.
									ALL RIGHT S RESERVED 대한무역투자진흥공사</span>
							</div>
							<div class="p2_family_site">
								<button class="p2_kotra_btn">
									<span>Kotra</span>
								</button>
								<button class="p2_gep_btn">
									<span>GEP</span>
								</button>
								<button class="p2_bk_btn">
									<span>buy Korea</span>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- view-wrap E -->
</body>
</html>
