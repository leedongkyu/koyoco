<%--
  Created by IntelliJ IDEA.
  User: 코요코3
  Date: 2021-12-18
  Time: 오후 1:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="ko">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="format-detection" content="telephone=no">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<title>코요코</title>
<jsp:include page="/WEB-INF/layout/header_search.jsp"></jsp:include>
</head>
<script type="text/javascript" src="${CONTEXT}/resources/js/board/thesis.js"></script>
<body>
  <div id="skip-menu">
    <a href="#gnb"><span>메인메뉴 바로가기</span></a>
    <a href="#snb"><span>서브메뉴 바로가기</span></a>
    <a href="#content"><span>컨텐츠 바로가기</span></a>
  </div>
  <div id="content-wrap"> <!-- content-wrap S -->
  <div id="content">
        <div class="popularity-box">
            <span>인기검색어</span>
            <a href="javascript:;"><span>인기검색어1</span></a>
            <a href="javascript:;"><span>인기검색어2</span></a>
            <a href="javascript:;"><span>인기검색어3</span></a>
            <a href="javascript:;"><span>인기검색어4</span></a>
        </div>
        <div class="cmm-tab">
            <a class="active" href="javascript:;"><span>전체</span></a>
            <a href="javascript:;"><span>논문</span></a>
            <a href="javascript:;"><span>저자</span></a>
            <a href="javascript:;"><span>메뉴얼</span></a>
        </div>
<!--         <h1 class="page-tit">검색결과<span class="num">230</span></h1> -->
        <div class="con-box">
            <h2>논문/학술/세미나 (<span class="con-num">${boardCount }</span>)</h2>
            <c:forEach items="${board}" var="board">
	            <button class="btn-icon plus"><span>더보기</span></button>
            	<div class="con-list">
	                <p>${board.title}</p>
	                <div class="cmm-flex">
	                    <strong>저자 <span class="con-name">${board.author}</span> 등록자 <span class="con-name2">${board.regUser}</span> 등록일 <span class="con-date">${board.regDate}</span> 가격 <span class="con-price">&#8361;${board.price}</span></strong>
	                    <label class="checkbox like">
	                        <input type="checkbox">
	                    </label>
	                    <button class="btn-icon download"><span>다운로드</span></button>
	                </div>
	            </div>
            </c:forEach>
        </div>
    </div>
  </div> <!-- content-wrap E -->
</body>
</html>
