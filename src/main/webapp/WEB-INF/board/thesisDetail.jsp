<%--
  Created by IntelliJ IDEA.
  User: 코요코3
  Date: 2021-12-18
  Time: 오후 1:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="ko">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="format-detection" content="telephone=no">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <link rel="shortcut icon" href="../../../resources/img/favicon.ico" type="image/x-icon">
  <title>Guide > Sample > Layout</title>
  <link rel="stylesheet" href="../../../resources/css/admin.css">
  <script type="text/javascript" src="../../../resources/js/publisher.js"></script> <!-- Layout 구성을 위한 퍼블리싱 스크립트 개발 적용 불 필요 -->
  <script type="text/javascript" src="../../../resources/js/commonui.js"></script>

  <link rel="stylesheet" href="../../../resources/css/oeuhwk.css">
</head>

<body>
<div id="view-wrap" aria-hidden="false" class="layout-guide"> <!-- view-wrap S -->
  <div id="skip-menu">
    <a href="#gnb"><span>메인메뉴 바로가기</span></a>
    <a href="#snb"><span>서브메뉴 바로가기</span></a>
    <a href="#content"><span>컨텐츠 바로가기</span></a>
  </div>
  <div id="header">
    <div class="top-box">
      <a class="logo" href="javascript:;"><span>KEP</span></a>
      <span class="user"><strong>코트라</strong>님 안녕하세요</span>
      <a class="txt" href="javascript:;"><span>로그아웃</span></a>
      <a class="notice new" href="javascript:;"><span>알림</span></a>
      <a class="search" href="javascript:;"><span>검색</span></a>
    </div>
  </div>
  <div id="content-wrap"> <!-- content-wrap S -->
    <div id="snb" class="toggle-box">
      <button type="button" class="btn-allmenu"><span>전체메뉴 펼치기</span></button>
      <button type="button" aria-expanded="false"><span>Menu01</span></button>
      <div class="view-box">
        <a href="javascript:;"><span>Sub01</span></a>
        <a href="javascript:;"><span>Sub01</span></a>
        <a href="javascript:;"><span>Sub01</span></a>
        <a href="javascript:;"><span>Sub01</span></a>
      </div>
      <button type="button" aria-expanded="false"><span>Menu02</span></button>
      <div class="view-box">
        <a href="javascript:;"><span>Sub01</span></a>
        <a href="javascript:;"><span>Sub01</span></a>
        <a href="javascript:;"><span>Sub01</span></a>
        <a href="javascript:;"><span>Sub01</span></a>
      </div>
      <button type="button" aria-expanded="false"><span>Menu03</span></button>
      <div class="view-box">
        <a href="javascript:;"><span>Sub01</span></a>
        <a href="javascript:;"><span>Sub01</span></a>
        <a href="javascript:;"><span>Sub01</span></a>
        <a href="javascript:;"><span>Sub01</span></a>
      </div>
      <button type="button" aria-expanded="false"><span>Menu04</span></button>
      <div class="view-box">
        <a href="javascript:;"><span>Sub01</span></a>
        <a href="javascript:;"><span>Sub01</span></a>
        <a href="javascript:;"><span>Sub01</span></a>
        <a href="javascript:;"><span>Sub01</span></a>
      </div>
      <button type="button" aria-expanded="false"><span>Menu05</span></button>
      <div class="view-box">
        <a href="javascript:;"><span>Sub01</span></a>
        <a href="javascript:;"><span>Sub01</span></a>
        <a href="javascript:;"><span>Sub01</span></a>
        <a href="javascript:;"><span>Sub01</span></a>
      </div>
      <button type="button" aria-expanded="true"><span>Menu06</span></button>
      <div class="view-box">
        <a href="javascript:;"><span>Sub01</span></a>
        <a href="javascript:;"><span>Sub01</span></a>
        <a href="javascript:;"><span>Sub01</span></a>
        <a href="javascript:;"><span>Sub01</span></a>
      </div>
    </div>
    <div id="content">
      <div class="sub-content">
        <div class="toggle-box">
          <button type="button" aria-expanded="true"><span>회원정보</span></button>
          <div class="view-box">
            <div class="cmm-table">
              <table>
                <caption>회원정보</caption>
                <colgroup>
                  <col style="width:15%">
                  <col style="width:35%">
                  <col style="width:15%">
                  <col style="width:35%">
                </colgroup>
                <tbody class="t-left">
                <tr>
                  <th scope="row">국가</th>
                  <td>KR</td>
                  <th scope="row">아이디(ID)</th>
                  <td>${member.userId}</td>
                </tr>
                <tr>
                  <th scope="row">회원구분</th>
                  <td>KOTRA</td>
                  <th scope="row">비밀번호</th>
                  <td></td>
                </tr>
                <tr>
                  <th scope="row">이름(국문)</th>
                  <td>${member.userName}</td>
                  <th scope="row">이름(영문)</th>
                  <td>이 캉 현</td>
                </tr>
                <tr>
                  <th scope="row">회원유형</th>
                  <td>개인 / 국내기업 / 해외기업</td>
                  <th scope="row">상태</th>
                  <td>사용 / 휴면 / 탈퇴</td>
                </tr>
                <tr>
                  <th scope="row">이메일</th>
                  <td></td>
                  <th scope="row">휴대폰번호</th>
                  <td></td>
                </tr>
                <tr>
                  <th scope="row">가입일</th>
                  <td></td>
                  <th scope="row">최종방문일/탈퇴일</th>
                  <td></td>
                </tr>
                <tr>
                  <th scope="row">개인정보마케팅활용동의</th>
                  <td></td>
                  <th scope="row"></th>
                  <td></td>
                </tr>
                <tr>
                  <th scope="row">프로필 사진</th>
                  <td colspan="3">
                      <span class="img">
<%--                        <img src="../../resources/img/@draft/01.png" alt="">--%>
                        </span>
                  </td>
                </tr>
                </tbody>
              </table>
              <div class="btn-box">
                <button type="button" class="btn-blue"><span>레이어 팝업 호출</span></button>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div> <!-- content-wrap E -->
  <div class="p2_main_footer">
    <div class="p2_size_content">
      <div class="p2_footer_logo_box">
        <a href="#" class="p2_kep_logo">
          <img src="../../resources/img/00_oeuhwk/img_logo_gray.png" alt="logo">
        </a>
      </div>
      <div class="p2_footer_content_box">
        <div class="p2_footer_list">
          <a href=""><span>전시회</span></a>
          <a href=""><span>KEP소개</span></a>
          <a href=""><span>지원센터</span></a>
        </div>
        <div class="p2_footer_content">
          <div class="p2_share_box">
            <button class="p2_share_mail">
              <i></i>
              <span>뉴스레터 구독</span>
            </button>
            <button class="p2_share_facebook">
              <span>facebook</span>
            </button>
            <button class="p2_share_linkedin">
              <span>linkedin</span>
            </button>
            <button class="p2_share_twitter">
              <span>twitter</span>
            </button>
            <button class="p2_share_instagram">
              <span>instagram</span>
            </button>
          </div>
          <div class="p2_footer_btn_term">
            <button><span>이용약관</span></button>
            <hr>
            <button><span>저작권정책</span></button>
            <hr>
            <button class="p2_active"><span>개인정보처리 방침</span></button>
          </div>
          <div class="p2_footer_info_box">
            <div class="p2_footer_address">
              <span>067792) 서울시 서초구 헌릉로 13</span>
              <span>사업자등록번호 120-82-00275</span>
              <span>TEL 1600-7119</span>
            </div>
            <span class="p2_footer_copyright">COPYRIGHT(c)2021 KOTRA. ALL RIGHT S RESERVED 대한무역투자진흥공사</span>
          </div>
          <div class="p2_family_site">
            <button class="p2_kotra_btn">
              <span>Kotra</span>
            </button>
            <button class="p2_gep_btn">
              <span>GEP</span>
            </button>
            <button class="p2_bk_btn">
              <span>buy Korea</span>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> <!-- view-wrap E -->
</body>
</html>
