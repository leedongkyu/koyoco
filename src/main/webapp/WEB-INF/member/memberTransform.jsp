<%--
  Created by IntelliJ IDEA.
  User: 코요코3
  Date: 2021-12-18
  Time: 오후 1:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="ko">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" href="${CONTEXT}/resources/img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="${CONTEXT}/resources/css/oeuhwk.css">
    <jsp:include page="/WEB-INF/layout/header_search.jsp"></jsp:include>


    <div id="content-wrap"> <!-- content-wrap S -->
        <div id="content">
            <div class="sub-content">
                <div class="toggle-box">
                    <button type="button" aria-expanded="true"><span>회원정보</span></button>
                    <div class="view-box">
                        <div class="cmm-table">
                            <table>
                                <caption>회원정보</caption>
                                <colgroup>
                                    <col style="width:15%">
                                    <col style="width:35%">
                                    <col style="width:15%">
                                    <col style="width:35%">
                                </colgroup>
                                <tbody class="t-left">
                                <tr>
                                    <th scope="row">국가</th>
                                    <td>KR</td>
                                    <th scope="row">아이디(ID)</th>
                                    <td>${memberTrf.userId}</td>
                                </tr>
                                <tr>
                                    <th scope="row">회원구분</th>
                                    <td>KOTRA</td>
                                    <th scope="row">비밀번호</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th scope="row">이름(국문)</th>
                                    <td>${memberTrf.userNm}</td>
                                    <th scope="row">이름(영문)</th>
                                    <td>이 캉 현</td>
                                </tr>
                                <tr>
                                    <th scope="row">회원유형</th>
                                    <td>
                                        <div class="cmm-form required" style="width: 90%;display:-webkit-inline-box;">
                                        <select title="회원유형" id="manageTp">
                                            <option value="1">병원</option>
                                            <option value="2">기업</option>
                                        </select>
                                        </div>
                                    </td>
                                    <th scope="row">상태</th>
                                    <td>사용 / 휴면 / 탈퇴</td>
                                </tr>
                                <tr>
                                    <th scope="row">이메일</th>
                                    <td>${memberTrf.userEmail}</td>
                                    <th scope="row">휴대폰번호</th>
                                    <td>${memberTrf.userPhone}</td>
                                </tr>
                                <tr>
                                    <th scope="row">병원이름/기업이름</th>
                                    <td>
                                        <div class="cmm-form required" style="width: 90%;display:-webkit-inline-box;">
                                            <input type="text" id="trsNm" name="trsNm" title="병원이름/기업이름" value="">
                                        </div>
                                    </td>
                                    <th scope="row">대표명</th>
                                    <td>
                                        <div class="cmm-form required" style="width: 90%;display:-webkit-inline-box;">
                                            <input type="text" id="seqNm" name="seqNm" title="대표명" value="">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">사업자번호/병원번호</th>
                                    <td colspan="3">
                                        <div class="cmm-form required" style="width: 90%;display:-webkit-inline-box;">
                                            <input type="text" id="seqNumber" name="seqNumber" title="사업자병원번호" value="">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">프로필 사진</th>
                                    <td colspan="3">
                      <span class="img">
                          <%--                        <img src="../../resources/img/@draft/01.png" alt="">--%>
                      </span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="btn-box">
                                <button type="button" class="btn-blue" id="btnSave"><span>회원전환신청</span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div> <!-- content-wrap E -->
    <div class="p2_main_footer">
        <div class="p2_size_content">
            <div class="p2_footer_logo_box">
                <a href="#" class="p2_kep_logo">
                    <img src="${CONTEXT}/resources/img/00_oeuhwk/img_logo_gray.png" alt="logo">
                </a>
            </div>
            <div class="p2_footer_content_box">
                <div class="p2_footer_list">
                    <a href=""><span>전시회</span></a>
                    <a href=""><span>KEP소개</span></a>
                    <a href=""><span>지원센터</span></a>
                </div>
                <div class="p2_footer_content">
                    <div class="p2_share_box">
                        <button class="p2_share_mail">
                            <i></i>
                            <span>뉴스레터 구독</span>
                        </button>
                        <button class="p2_share_facebook">
                            <span>facebook</span>
                        </button>
                        <button class="p2_share_linkedin">
                            <span>linkedin</span>
                        </button>
                        <button class="p2_share_twitter">
                            <span>twitter</span>
                        </button>
                        <button class="p2_share_instagram">
                            <span>instagram</span>
                        </button>
                    </div>
                    <div class="p2_footer_btn_term">
                        <button><span>이용약관</span></button>
                        <hr>
                        <button><span>저작권정책</span></button>
                        <hr>
                        <button class="p2_active"><span>개인정보처리 방침</span></button>
                    </div>
                    <div class="p2_footer_info_box">
                        <div class="p2_footer_address">
                            <span>067792) 서울시 서초구 헌릉로 13</span>
                            <span>사업자등록번호 120-82-00275</span>
                            <span>TEL 1600-7119</span>
                        </div>
                        <span class="p2_footer_copyright">COPYRIGHT(c)2021 KOTRA. ALL RIGHT S RESERVED 대한무역투자진흥공사</span>
                    </div>
                    <div class="p2_family_site">
                        <button class="p2_kotra_btn">
                            <span>Kotra</span>
                        </button>
                        <button class="p2_gep_btn">
                            <span>GEP</span>
                        </button>
                        <button class="p2_bk_btn">
                            <span>buy Korea</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- view-wrap E -->


<script type="text/javascript">


    $(document).ready(function() {
        $("#btnSave").on("click", function () {
            saveButtonOnClick();
        });
    });

    function lfn_callBack(data, ID){
        if (ID == 'save') {
            if (data.rtn == 'success') {
                alert('저장 성공');
                location.href = "/member/detail/user";
            } else {
                alert('실패');
            }
        }
    };

    function saveButtonOnClick() {

        var param = JSON.stringify({seqNum:$("#manageTp option:selected").val(),trsNm : $("#trsNm").val(),seqNm :$("#seqNm").val(),seqNumber : $("#seqNumber").val()});
        gfn_ajaxTransmit("/member/detail/user/transInsert", param, "save")
    }

</script>
</body>
</html>
