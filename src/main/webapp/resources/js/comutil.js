/**
 * JavaScript replaceAll - 정규식 사용
 * replaceAll: 주어진 문자열에서 특정 모든 문자를 다른 문자로 변경.
 * parameters
 *  - fullStr: 전체 문자열
 *  - str1: 바꾸고자 하는 문자열
 *  - str2: 변경될 문자열
 *  - ignore: case sensitive 고려여부 (true > ignore case sensitive, false > case sensitive)
 *  ex) var numStr = replaceAll("1-2-3-4-5-6-7","-","");
 */
function gfn_replaceAll(fullStr,str1, str2, ignore) {
	if (gfn_isNull(fullStr)) return "";
	return fullStr.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),	(ignore?"gi":"g")),(typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2);
}

/**
 * =======================================================================================
 * 공백 체크(NULL 포함)
 * =======================================================================================
 */
$.fn.isEmpty = function() {
	
	var $parent = $(this).parent();
	var $require = $parent.hasClass('required');
	var $alert = $parent.children('p.alert');
	var $label = $parent.children('.label');

	if($(this).is('select')) {
		// null check
		if($require){
			$alert.remove();
			$parent.removeClass('alert');
			var val = $(this).children('option:selected').val();
			if( val == null || val == '' ) {
				$parent.addClass('alert');
				$parent.append("<p class=alert>"+$label.text()+'은(는) 필수 입력 항목입니다.'+"</p>");
				$(this).focus();
				return true;
			}
			// space check
			if ($.trim(val) == null || $.trim(val) == '' ) {
				$parent.addClass('alert');
				$parent.append("<p class=alert>"+$label.text()+'은(는) 필수 입력 항목입니다.'+"</p>");
				$(this).focus();
				return true;
			}
		}
	}
	else {
		// input type : radio
		if ($(this).attr('type') === 'radio') {
			var $cParent = $(this).parent().parent();
			var $cLabel = $cParent.children('.label');
			if($cParent.hasClass('required')){
				$cParent.children('p.alert').remove();
				$cParent.removeClass('alert');
				if( $('input:radio[name='+$(this).attr('name')+']:checked').length < 1) {
					$cParent.addClass('alert');
					$cParent.append("<p class=alert>"+$cLabel.text()+'은(는) 필수 입력 항목입니다.'+"</p>");
					$(this).focus();
					return true;
				}
			}
		// input type : checkbox
		} else if ($(this).attr('type') === 'checkbox') {

			if($require){
				$alert.remove();
				$parent.removeClass('alert');
				if( $('input:checkbox[name='+$(this).attr('name')+']:checked').length < 1) {
					$parent.addClass('alert');
					$parent.append("<p class=alert>"+$label.text()+'은(는) 필수 입력 항목입니다.'+"</p>");
					$(this).focus();
					return true;
				}
			}
			
		} else if($(this).attr('type') === 'file'){
			
			var $fParent = $(this).parent().parent();
			var $fLabel = $fParent.children('.label');
			if($fParent.hasClass('required')){
				$fParent.children('p.alert').remove();
				$fParent.removeClass('alert');
				if($fParent.children().hasClass('cmm-thum-list')){
					if($fParent.children('.cmm-thum-list').children('li').length < 1){
						$fParent.addClass('alert');
						$fParent.append("<p class=alert>"+$fLabel.text()+'은(는) 파일을 필수로 첨부해야합니다.'+"</p>");
						$(this).focus();
						return true;
					}
				}else{
					
					if($(this).val() == ''){

						$fParent.addClass('alert');
						$fParent.append("<p class=alert>"+$fLabel.text()+'은(는) 파일을 필수로 첨부해야합니다.'+"</p>");
						$(this).focus();
						return true;
					}
					
					if($.trim($(this).val()) == ''){

						$fParent.addClass('alert');
						$fParent.append("<p class=alert>"+$fLabel.text()+'은(는) 파일을 필수로 첨부해야합니다.'+"</p>");
						$(this).focus();
						return true;
						
					}
				}
				
			}
			
		} else {
			if($require){
				
				$alert.remove();
				$parent.removeClass('alert');
				
				
				if($(this).is('textarea')){
					// smartEditor null check
					if( $(this).val() == "<p><br></p>") {
						$parent.addClass('alert');
						$parent.append("<p class=alert>"+$label.text()+'은(는) 필수 입력 항목입니다.'+"</p>");
						$(this).focus();
						return true;
					}
				}
				
				// null check
				if( $(this).val() == '' || $(this).val() == null  ) {
					$parent.addClass('alert');
					$parent.append("<p class=alert>"+$label.text()+'은(는) 필수 입력 항목입니다.'+"</p>");
					$(this).focus();
					return true;
				}
				
				// space check
				if ( $.trim($(this).val()) == '' || $.trim($(this).val()) == null ) {
					$parent.addClass('alert');
					$parent.append("<p class=alert>"+$label.text()+'은(는) 필수 입력 항목입니다.'+"</p>");
					$(this).focus();
					return true;
				}
			}else if($(this).parent().parent().hasClass('required')){
				
				
				var $fParent = $(this).parent().parent();
				var $fLabel = $fParent.children('.label');
				
				$fParent.children('p.alert').remove();
				$fParent.removeClass('alert');
				
				if($(this).val() == ''){

					$fParent.addClass('alert');
					$fParent.append("<p class=alert>"+$fLabel.text()+'은(는) 필수 입력 항목입니다.'+"</p>");
					$(this).focus();
					return true;
				}
				
				if($.trim($(this).val()) == ''){

					$fParent.addClass('alert');
					$fParent.append("<p class=alert>"+$fLabel.text()+'은(는) 필수 입력 항목입니다.'+"</p>");
					$(this).focus();
					return true;
				}
			}
		}
	}
	
	return false;
};

/**
 * ==================================================================================
 * 폼 벨리데이션 체크
 * ----------------------------------------------------------------------------------
 * 사용법
 * 	- data-valid-empty="true" 			: NULL 체크
 * ==================================================================================
 */
$.fn.formValid = function() {
	var $obj = $(this);
	var rtnFlag = true;
	$obj.find('input, select, textarea').each(function(){
		if ($(this).is('input')) {
			var type = $(this).attr('type');
			// input type : radio
			if (type == 'radio') {
				if( $(this).isEmpty() == true ) {
					rtnFlag = false;
					return false; // out each
				};
			// input type : checkbox
			}else if (type == 'checkbox') {
				if( $(this).isEmpty() == true ) {
					rtnFlag = false;
					return false; // out each
				};
			// input type : file
			}else if (type == 'file'){
				if( $(this).isEmpty() == true ) {
					rtnFlag = false;
					return false; // out each
				};
				// input type : other
			}else {
				if( $(this).isEmpty() == true ) {
					rtnFlag = false;
					return false; // out each
				};
			};
		}else if ($(this).is('select')) {
			if( $(this).isEmpty() == true ) {
				rtnFlag = false;
				return false; // out each
			};
		}else if ($(this).is('textarea')) {
			if( $(this).isEmpty() == true ) {
				rtnFlag = false;
				return false; // out each
			};
		};

	});
	return rtnFlag;
};

/**
 * serializeObject - form data -> Object 
 */
$.fn.serializeObject = function() {
	var obj = null;

	try {
		//this[0].tagName이 form tag일 경우
		if(this[0].tagName && this[0].tagName.toUpperCase() == "FORM" ) {
			var arr = this.serializeArray();
			if(arr){

				obj = {};    

				$.each(arr, function() {
					//obj의 key값은 arr의 name, obj의 value는 value값
					obj[this.name] = this.value;
				});				
			}
		}
	}catch(e) {
		alert(e.message);
	}
		return obj;
	};


/**
 * =======================================================================================
 * 공통 AJAX 처리(POST)
 * =======================================================================================
 */
function gfn_ajaxTransmit(url, param, ID) {
	
    $.ajax({
    	 url         : url
       , type        : "post"
       , data        : param
       , dataType    : "json"
       , contentType: "application/json; charset:UTF-8"
       , success     : function(data) {
    	   lfn_callBack(data, ID)
       },
       error         : function(xhr, errorName, error) {
           alert("에러입니다." + xhr.statusText);
       }
    });
}    




