
/****************************** Layer Popup Open & Close S ******************************/
    // Open
    $(document).on('click', '*[data-layer-target]', function(){
        var target = $(this).attr('data-layer-target');
        $('body').addClass('scroll-off').find('#view-wrap').attr('aria-hidden','true').siblings('*').attr('aria-hidden','true');
        $('#' + target).addClass('active').attr('aria-hidden','false').focus();
    });
    // Close
    function layerClose(){
        var target = $('*[aria-hidden="false"].cmm-layer');
            targetID = target.attr('id');
        if($('*[data-layer-target="'+ targetID +'"]').parents('div').hasClass('.cmm-layer')){
            $('body').addClass('scroll-off').find('#view-wrap').attr('aria-hidden','true').siblings('.cmm-layer').attr('aria-hidden','true');
        } else {
            $('body').removeClass('scroll-off').find('#view-wrap').attr('aria-hidden','false').siblings('.cmm-layer').attr('aria-hidden','false');
        }
        target.removeClass('active').attr('aria-hidden','true');
        $('*[data-layer-target="'+ targetID +'"]').focus();
    }
    $(document).on('click', '.cmm-layer .dim, .cmm-layer .close, #close', function(){
        layerClose();
    });

    // Focus
    /*
    $(document).on('keydown', '.cmm-layer .dim', function(e){
        if(e.keyCode == 9){
            $(this).closest('.cmm-layer').find('.header').find('.close').focus();
        }
    });
    $(document).on('keydown', '.cmm-layer .header .close', function(e){
        if(e.keyCode == 9 && e.shiftKey){
            $(this).parents('.cmm-layer').find('.dim').focus();
        }
    });
    */
/****************************** Layer Popup Open & Close E ******************************/
