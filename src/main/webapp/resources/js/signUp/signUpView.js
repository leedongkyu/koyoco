$(document).ready(function(){
	
	FORM_DATA = {};
	//공통코드 조회
	signUpView.lfn_Code();
	
	$('#lfn_save').click(function(){
		signUpView.lfn_save();
	});
	
});

var signUpView = {
		   //입력
		   lfn_save : function(){
			  if($('#form').formValid()){
				   var data = $('#form').serializeObject();
				   
				   var param = JSON.stringify(data);
				   gfn_ajaxTransmit("/signUp/signUpEnd", param, "save")
			   }
			},
		   
		   //조회
		   lfn_select : function(paramData){
			   console.log(paramData);
		   },
		   
		   // 공통코드 호출시
		   lfn_Code : function(){

		   },
};

function lfn_callBack(data, ID){
	   if (ID == 'search') {
		   
	   } else if (ID == 'save') {
			console.log(data);
			location.href="/signUp/login";
	   }
};
