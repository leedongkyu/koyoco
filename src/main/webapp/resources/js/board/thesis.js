$(document).ready(function(){
	$('#con-num').text();
});

// 조회 ajax
/*function searchButtonOnClick() {
	if ($('#form').formValid()) {
		var data = $('#form').serializeObject();
		var param = JSON.stringify(data);
		gfn_ajaxTransmit("/board/thesis/search", param, "search")
	}
};*/

function lfn_callBack(data, ID){
	if (ID == 'search') {
		searchCallBack(data);
	}
};

// 조회 후 실행
function searchCallBack(data) {
	$('.board-content div').remove();
	if (data.length == 0) {
		$('#searchcount').text('0');
	} else {
		$('#searchcount').text(data.length);
		var str = ""
		for (var i = 0; i < data.length; i++) {
			str += "<div style='margin: 15px;'>"
			str += "<div style='display: flex;'>"
			str += "<span style='font-size: 30px; float: left;' id='title'>"+ data[i].title +"</span>"
			str += "</div>"
			str += "<span style='font-size: 15px; float: left;' id='title'>저자 : "+ data[i].author +"</span>"
			str += "<span style='font-size: 15px; float: left;' id='title'>등록자 : "+ data[i].regUser +"</span>"
			str += "<span style='font-size: 15px; float: left;' id='title'>등록일 : "+ data[i].regDate +"</span>"
			str += "<span style='font-size: 15px; float: left;' id='title'>가격 : "+ data[i].price +"</span>"
			str += "</div>"
		}
		$('.board-content').append(str);
	}
}
