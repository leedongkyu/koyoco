var now = new Date();	// 현재 날짜 및 시간
var year = now.getFullYear();	// 연도

$(document).ready(function(){
	$("#btnSave").on("click", function(){
		saveButtonOnClick();
	});
});


function saveButtonOnClick() {
	if ($('#form').formValid()) {
		if ($('#THESIS_YEAR').val() > year) {
			alert('발행연도 값이 잘못 들어가있습니다.');
			return false;
		}
		console.log($('#THESIS_YEAR'));
		var data = $('#form').serializeObject();
		var param = JSON.stringify(data);
		gfn_ajaxTransmit("/board/thesis/register/save", param, "save")
	}
}

function lfn_callBack(data, ID){
	if (ID == 'save') {
		if (data.rtn == 'success') {
			var url = "http://localhost:8080/board/thesis/page";
			$(location).attr('href',url);
		} else {
			alert('실패');
		}
	}
};