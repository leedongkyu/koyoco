/**
 * =======================================================================================
 * 공통 사용 스크립트 모음
 * ---------------------------------------------------------------------------------------
 * object 접근형
 * ---------------------------------------------------------------------------------------
 * 	- isEmpty							공백 체크(NULL 포함)
 * ---------------------------------------------------------------------------------------
 * function 호출형
 * ---------------------------------------------------------------------------------------
 *  - cf_bizGetEncode(send_data)		비지니스와 API 통신시 데이터 컨버팅 GET
 *  - cf_bizPostEncode(send_data)		비지니스와 API 통신시 데이터 컨버팅 POST
 * 	- cf_isEmpty(val)					공백 체크(NULL 포함)
 * 	- cf_replaceAll(val,str1,str2)		값 중 str1을 str2로 변경
 * 	- cf_getFileExt(val)				값의 파일 확장자 구함
 * 	- cf_xssFilter(val)					XSS Filter (< , > -> &lt; , &gt;)
 * =======================================================================================
 */


/**
 * =======================================================================================
 * 공백 체크(NULL 포함)
 * =======================================================================================
 */
$.fn.isEmpty = function() {
	
	var $parent = $(this).parent();
	var $require = $parent.hasClass('required');
	var $alert = $parent.children('p.alert');
	var $label = $parent.children('.label');

	if($(this).is('select')) {
		// null check
		if($require){
			
			$alert.remove();
			$parent.removeClass('alert');
			
			var val = $(this).children('option:selected').val();
			if( val == null || val == '' ) {
				$parent.addClass('alert');
				$parent.append("<p class=alert>"+$label.text()+'은(는) 필수 입력 항목입니다.'+"</p>");
				$(this).focus();
				return true;
			}
			// space check
			if ($.trim(val) == null || $.trim(val) == '' ) {
				$parent.addClass('alert');
				$parent.append("<p class=alert>"+$label.text()+'은(는) 필수 입력 항목입니다.'+"</p>");
				$(this).focus();
				return true;
			}
		}
	}
	else {
		// input type : radio
		if ($(this).attr('type') === 'radio') {
			var $cParent = $(this).parent().parent();
			var $cLabel = $cParent.children('.label');
			
			if($cParent.hasClass('required')){
				$cParent.children('p.alert').remove();
				$cParent.removeClass('alert');
				
				
				if( $('input:radio[name='+$(this).attr('name')+']:checked').length < 1) {
					$cParent.addClass('alert');
					$cParent.append("<p class=alert>"+$cLabel.text()+'은(는) 필수 입력 항목입니다.'+"</p>");
					$(this).focus();
					return true;
				}
				
			}
		// input type : checkbox
		} else if ($(this).attr('type') === 'checkbox') {

			if($require){
				
				$alert.remove();
				$parent.removeClass('alert');
				
				if( $('input:checkbox[name='+$(this).attr('name')+']:checked').length < 1) {
					$parent.addClass('alert');
					$parent.append("<p class=alert>"+$label.text()+'은(는) 필수 입력 항목입니다.'+"</p>");
					$(this).focus();
					return true;
				}
			}
			
		} else if($(this).attr('type') === 'file'){
			
			var $fParent = $(this).parent().parent();
			var $fLabel = $fParent.children('.label');
			if($fParent.hasClass('required')){
				
				$fParent.children('p.alert').remove();
				$fParent.removeClass('alert');
				
				if($fParent.children().hasClass('cmm-thum-list')){
					if($fParent.children('.cmm-thum-list').children('li').length < 1){

						$fParent.addClass('alert');
						$fParent.append("<p class=alert>"+$fLabel.text()+'은(는) 파일을 필수로 첨부해야합니다.'+"</p>");
						$(this).focus();
						return true;
					}
				}else{
					
					if($(this).val() == ''){

						$fParent.addClass('alert');
						$fParent.append("<p class=alert>"+$fLabel.text()+'은(는) 파일을 필수로 첨부해야합니다.'+"</p>");
						$(this).focus();
						return true;
					}
					
					if($.trim($(this).val()) == ''){

						$fParent.addClass('alert');
						$fParent.append("<p class=alert>"+$fLabel.text()+'은(는) 파일을 필수로 첨부해야합니다.'+"</p>");
						$(this).focus();
						return true;
						
					}
				}
				
			}
			
		} else {
			if($require){
				
				$alert.remove();
				$parent.removeClass('alert');
				
				
				if($(this).is('textarea')){
					// smartEditor null check
					if( $(this).val() == "<p><br></p>") {
						$parent.addClass('alert');
						$parent.append("<p class=alert>"+$label.text()+'은(는) 필수 입력 항목입니다.'+"</p>");
						$(this).focus();
						return true;
					}
				}
				
				// null check
				if( $(this).val() == '' || $(this).val() == null  ) {
					$parent.addClass('alert');
					$parent.append("<p class=alert>"+$label.text()+'은(는) 필수 입력 항목입니다.'+"</p>");
					$(this).focus();
					return true;
				}
				
				// space check
				if ( $.trim($(this).val()) == '' || $.trim($(this).val()) == null ) {
					$parent.addClass('alert');
					$parent.append("<p class=alert>"+$label.text()+'은(는) 필수 입력 항목입니다.'+"</p>");
					$(this).focus();
					return true;
				}
			}else if($(this).parent().parent().hasClass('required')){
				
				
				var $fParent = $(this).parent().parent();
				var $fLabel = $fParent.children('.label');
				
				$fParent.children('p.alert').remove();
				$fParent.removeClass('alert');
				
				if($(this).val() == ''){

					$fParent.addClass('alert');
					$fParent.append("<p class=alert>"+$fLabel.text()+'은(는) 필수 입력 항목입니다.'+"</p>");
					$(this).focus();
					return true;
				}
				
				if($.trim($(this).val()) == ''){

					$fParent.addClass('alert');
					$fParent.append("<p class=alert>"+$fLabel.text()+'은(는) 필수 입력 항목입니다.'+"</p>");
					$(this).focus();
					return true;
					
				}
			}
		}
	}
	
	return false;
};



$.fn.renderChart = function(data,options,type){
	
	var el = document.getElementById($(this).attr('id'));
	var data = data;
	var options = options;
	var Chart = tui.chart;

	switch(type){
		case 'area': return Chart.areaChart(el, data, options);
		break;
		case 'line': return Chart.lineChart(el, data, options);
		break;
		case 'lineArea': return Chart.lineArea(el, data, options);
		break;
		case 'bar': return Chart.barChart(el, data, options);
		break;
		case 'column': return Chart.colmunChart(el, data, options);
		break;
		case 'columnLine': return Chart.colmunLineChart(el, data, options);
		break;
		case 'bulletLine': return Chart.bulletChart(el, data, options);
		break;
		case 'treemap': return Chart.treemapChart(el, data, options);
		break;
		case 'heatmap': return Chart.heatmapChart(el, data, options);
		break;
		case 'scatter' : return Chart.scatterChart(el, data, options);
		break;
		case 'lineScatter' : return Chart.lineScatterChart(el, data, options);
		break;
		case 'bubble' : return Chart.bubbleChart(el, data, options);
		break;
		case 'pie' : return Chart.pieChart(el, data, options);
		break;
		case 'nestedPie' : return Chart.nestedPieChart(el, data, options);
		break;
		case 'radar' : return Chart.radarChart(el, data, options);
		break;	
		default:  return Chart.lineChart( el, data, options);
		break;
	} ;
	
};


/*
 * =============================================================================
 *   이미지 파일 첨부시 화면 이미지 랜더링 함수
 * ==============================================================================
 *  *PARAM
 * 	 obj        - 파일객체 [필수값]
 *   fileNode   - 파일노드요소  (파일노드복사)[필수값]
 *   type       - 이미지와 텍스트 함꼐 보여줄것인지 텍스트요소만 보여줄것인지(이미지포함인 경우 image로 파라미터 세팅)[필수값]
 * 	 nodeName   - 파일노드 생성시 지정할 요소 naming[필수값]
 *   limitMbyte - 제한 메가 바이트 수 (undefined 시 검사안함)[선택값]
 */

$.fn.imgFileRender = function(obj,fileNode,type,nodeName,limitMByte){
	
		var files = obj.target.files;
		var fileArr = Array.prototype.slice.call(files);
		var id = "#"+$(this).attr('id');
		var MbyteForByte = limitMByte*1000*1024;

		fileArr.forEach(function(f){
			if(type === 'image'){
				if(!f.type.match("image.*")){
					alert("확장자는 이미지 확장자만 가능합니다.");
					return ;
				}
			}
			
			
			if(limitMByte !== undefined){
				if(f.size>MbyteForByte){
					alert(MbyteForByte+"를 초과하는 파일은 첨부할 수 없습니다.");
					return;
				}
			}
			
			var fileName = f.name;
			var reader = new FileReader();
			
			reader.onload = function(e){

					var str = "<li>";
					if(type === 'image'){
						str += "<span class=img><img src="+e.target.result+"></span>";
					}
					str += "<p class=name>"+fileName+"</p>";
					str += "<button type=button onclick='"+'javascript:$(this).parent().remove()'+"'id=imgDelete class='"+'btn-icon delete'+"'><span>삭제</span></button>";
					str += "</li>";
				
					fileNode.attr("style","display:none");
					fileNode.attr("name",nodeName);
					fileNode.attr("data-append",'true');
				
					$(id).append(str);
					$(id).find("li:last").append(fileNode);

			}
			
			reader.readAsDataURL(f);
			
		});
};



$.fn.commonSelect = function(url,sdata){

	  
	  cf_simpleAjaxPostCall(url, JSON.stringify(sdata)).done(function(obj){

		  Object.keys(obj.data).foreach(function(key){
			  $(this).append('<option value='+obj.data[key]+'>'+key+'</option>');
		  });
		  
	  });
	
};


/*
 * =============================================================================
 *   상세조회후 데이터를 각요소의 값 세팅 / obj key와 요소의 name이 필치하여야 한다
 * ==============================================================================
 *  *PARAM
 *  obj - 결과데이터
 */
function formDataDtl(obj){
	
	Object.keys(obj).forEach(function(key){
		var key = key;
		var target = $("[name="+key+"]");
		try{
			var tagName = target[0].tagName;
			var value = obj[key];
			if(tagName == 'INPUT'){
				switch (target.prop('type')) {
				case 'text':
					target.val(value);
					break;
					
				case 'radio':
					$("[name="+key+"]").each(function(){
						$(this).val() == value ? $(this).attr('checked','checked') : '';
					});
					break;
				case 'checkbox':
					$("[name="+key+"]").each(function(){

						 if(Array.isArray(obj[key])){
							 for(var i = 0 ; i<obj[key].length ; i++){
									$(this).val() == obj[key][i] ? $(this).attr('checked','checked') : '';									 	
							 }						 
						 }else{
							 	$(this).val() == value ? $(this).attr('checked','checked') : '';
						 }
					});				
					break;
					
				case 'hidden':
					target.val(value);
					break;
					
				default:
					break;
				}
			}else if(tagName == 'SELECT'){
				if(target.prop('type') == 'select-one'){
					$(this).val() == value ? $(this).attr('selected','selected') : '';
				}else{
					 if(Array.isArray(obj[key])){
						 for(var i = 0 ; i<obj[key].length ; i++){
								$(this).val() == obj[key][i] ? $(this).attr('selected','selected') : '';									 	
						 }						 
					 }else{
						 	$(this).val() == value ? $(this).attr('selected','selected') : '';
					 }
				}
			}else if(tagName == 'TEXTAREA'){
					target.val(value);
			}
			
		} catch (e) {
			console.log(key+' / An element with that name does not exist.');
		}
		
		
	});
	
}

/**
 * =======================================================================================
 * 비지니스와 API 통신시 데이터 컨버팅 GET
 * ---------------------------------------------------------------------------------------
 * 기존 데이터를 json형태로 변경하고 url인코딩하여 리턴
 * ---------------------------------------------------------------------------------------
 * @param send_data
 * @returns
 * =======================================================================================
 */
function cf_bizGetEncode(send_data) {
	return encodeURI('p='+JSON.stringify(send_data));
}


/**
 * =======================================================================================
 * 비지니스와 API 통신시 데이터 컨버팅 POST
 * ---------------------------------------------------------------------------------------
 * 기존 데이터를 json형태로 변경하고 url인코딩하여 리턴
 * ---------------------------------------------------------------------------------------
 * @param send_data
 * @returns
 * =======================================================================================
 */
function cf_bizPostEncode(send_data) {
	return JSON.stringify(send_data);
}
/**
 * =======================================================================================
 * 공백 체크(NULL 포함)
 * ---------------------------------------------------------------------------------------
 * 데이터가 있으면 false
 * 데이터가 없으면 true
 * ---------------------------------------------------------------------------------------
 * @param val
 * @returns {Boolean}
 * =======================================================================================
 */
function cf_isEmpty(val) {
	if( val == null || val == '' || $.trim(val) == '' ) {
		return true;
	} else {
		return false;
	}
}

/**
 * =======================================================================================
 * 검색시 시작일자보다 종료일자가 과거인경우 체크 
 * ---------------------------------------------------------------------------------------
 * @param startDateId
 * @param endDateId
 * @returns {boolean}
 * =======================================================================================
 */

function dateValidation(startDateId,endDateId){
	
	
	var fromFlag = $('#'+startDateId).val();
	var toFlag = $('#'+endDateId).val();
	
	if( fromFlag && !toFlag ) {
		return false;
	}
	else if( !fromFlag && toFlag ) {
		return false;
	}
	
	
	if(fromFlag > toFlag){
		alert(dateMsg.vaild);
		return false;
	}else{
		return true
	}
	
}



/**
 * =======================================================================================
 * replaceAll 스크립트
 * ---------------------------------------------------------------------------------------
 * @param val
 * @param str1
 * @param str2
 * @returns {String}
 * =======================================================================================
 */
function cf_replaceAll(val, str1, str2) {
	return val.split(str1).join(str2);
}


/**
 * =======================================================================================
 * 파일 확장자 추출
 * ---------------------------------------------------------------------------------------
 * @param val
 * @returns {String}
 * =======================================================================================
 */
function cf_getFileExt(val) {
	
	var ext = '';
	var ext_length = val.lastIndexOf(".");
	ext = val.substring(ext_length + 1);
	ext = ext.toLowerCase();
	
	return ext;
}



/**
 * =======================================================================================
 * 파일 확장자 확인
 * ---------------------------------------------------------------------------------------
 * @param val
 * @returns {String}
 * =======================================================================================
 */
function cf_chkFileExt(ext, fileType) {
	
	for(var i = 0; i < fileType.length; i++) {
		if(ext == fileType[i]) {
			return true;
		}
	}
	return false;
}



/**
 * =======================================================================================
 * XSS 필터
 * ---------------------------------------------------------------------------------------
 * 값 중 < 와 > 를 &lt; , &gt;로 변경 처리
 * ---------------------------------------------------------------------------------------
 * @param val
 * @returns
 * =======================================================================================
 */
function cf_xssFilter(val) {
	return val.replace(/</g, "&lt;").replace(/>/g, "&gt;");
}



/**
 * =======================================================================================
 * 날자 형식 변경
 * ---------------------------------------------------------------------------------------
 * 1. yyyyMMdd - > yyyyMMdd
 * 2. yyyy?MM?dd - > yyyyMMdd
 * =======================================================================================
 */
$.fn.cvtDateDefault = function() {

	var value = $(this).val();
	
	if(value.length > 8) {
		
		var yyyy = value.substr(0,4);
		var mm = value.substr(5,2);
		var dd = value.substr(8,2);
		
		if(parseInt(mm) < 10) {
			mm = '0'+parseInt(mm);
		}
		if(parseInt(dd) < 10) {
			dd = '0'+parseInt(dd);
		}

		return yyyy + mm + dd;
		
	} else {
		
		return value;
		
	}
	
};



/**
 * =======================================================================================
 * 날자 형식 변경
 * ---------------------------------------------------------------------------------------
 * 1. yyyyMMdd - > yyyy-MM-dd
 * =======================================================================================
 */
$.fn.cvtDateHyphen = function() {

	var value = $(this).cvtDateDefault();
	var yyyy = value.substr(0,4);
	var mm = value.substr(4,2);
	var dd = value.substr(6,2);
	
	return yyyy + "-" + mm + "-" + dd;
	
};



/**
 * =======================================================================================
 * 날자 형식 변경
 * ---------------------------------------------------------------------------------------
 * 1. yyyyMMdd - > yyyy.MM.dd
 * =======================================================================================
 */
$.fn.cvtDateDot = function() {

	var value = $(this).cvtDateDefault();
	var yyyy = value.substr(0,4);
	var mm = value.substr(4,2);
	var dd = value.substr(6,2);
	
	return yyyy + "." + mm + "." + dd;
	
};



/**
 * =======================================================================================
 * 날자 형식 변경
 * ---------------------------------------------------------------------------------------
 * 1. yyyyMMdd - > yyyy/MM/dd
 * =======================================================================================
 */
$.fn.cvtDateSlash = function() {

	var value = $(this).cvtDateDefault();
	var yyyy = value.substr(0,4);
	var mm = value.substr(4,2);
	var dd = value.substr(6,2);
	
	return yyyy + "/" + mm + "/" + dd;
	
};



/**
 * =======================================================================================
 * 날자 형식 변경
 * ---------------------------------------------------------------------------------------
 * 1. yyyyMMdd - > yyyyMMdd
 * 2. yyyyMMdd - > yyyy/MM/dd
 * 3. yyyyMMdd - > yyyy-MM-dd
 * 4. yyyyMMdd - > yyyy.MM.dd
 * =======================================================================================
 */
$.fn.cvtDateFormat = function(format) {

	var cvt_date_value;

	if( format == 'yyyyMMdd' ) {
		cvt_date_value = $(this).cvtDateDefault();
	}else if( format == 'yyyy/MM/dd' ) {
		cvt_date_value = $(this).cvtDateSlash();
	}else if( format == 'yyyy-MM-dd' ) {
		cvt_date_value = $(this).cvtDateHyphen();
	}else if( format == 'yyyy.MM.dd' ) {
		cvt_date_value = $(this).cvtDateDot();
	}else {
		cvt_date_value = this.value;
	}
	
	return cvt_date_value;
	
};


function cf_cvtDateFormat(date, format) {
	
	if(typeof date == null || date == null || date == 'null' || date == '' ) {
		return '';
	}
	
	var tmp_obj = $('<input>');
	tmp_obj.attr('type','hidden');
	tmp_obj.attr('name','tmp');
	tmp_obj.attr('data-date-format',format);
	tmp_obj.val(date);
	
	var rtn_date = tmp_obj.cvtDateFormat();
	tmp_obj.remove();
	
	return rtn_date;
}

// 하이픈이 붙지않은 데이터를 하이픈을 붙인다.
function cf_cvtDateHyphen(value){
	
	var yyyy = value.substr(0,4);
	var mm = value.substr(4,2);
	var dd = value.substr(6,2);
	
	return yyyy + "-" + mm + "-" + dd;
	
}


// obj 데이터를 key=name이 같은 요소안에 값을 세팅해준다.
function cf_objToFormSet(obj){

	Object.keys(obj).forEach(function(key){
		var $element = $('[name='+key+']');
		
		if($element.attr('type') == 'text'){
			if($element.parent().hasClass('tui-datepicker-input')){
				$element.val(obj[key]);
			}else{
				$element.val(obj[key]);
			}
		}else if($element.attr('type') == 'radio'){

				$element.each(function(i){
					if($element[i].value == obj[key]){
						$element[i].checked = true;
					}
				});
		}else{
			$element.val(obj[key]);
		}
	});
	
}

// obj 데이터를 key=name이 같은 요소안에 값을 세팅해준다.(form 내부값이 아닌 일반 값에 세팅)
function cf_objToDataSet(obj){

	Object.keys(obj).forEach(function(key){
		var $element = $('.' + key);
		
		$element.html(obj[key]);
	});
}

function cf_appendZero(val, len) {
	var zero = '0';
    var rtn = String(val);
    for(var i = rtn.length; i < len; i++) {
    	rtn = zero + rtn;
    }
    return rtn;
}


// 메일 형식  체크
function lfn_isEmail(asValue) {

	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	return regExp.test(asValue); 
}


// 팝업창 호출 함수
function openPop(url, width, height) {
    var windowW = width;  // 창의 가로 길이
    var windowH = height;  // 창의 세로 길이
    var left = Math.ceil((window.screen.width - windowW)/2);
    var top = Math.ceil((window.screen.height - windowH)/2);
    var openWin = window.open(url,"pop","top="+top+", left="+left+", height="+windowH+", width="+windowW + ", resizable=true" );
}

//숫자 3자리 콤마
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

/**
 * 길이를 구한다 (한글은 3byte)
 */
function getTextLength(str) {
    var len = 0;
    for (var i = 0; i < str.length; i++) {
        if (escape(str.charAt(i)).length == 6) {
            len += 2;
        }
        len++;
    }
    return len;
}


// 패스워드 형식 체크

function cd_validatePassword(character) {
	
	var reg = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]*$/; // 문자 + 숫자 조합
	var reg1 = /^(?=.*[!@#$%^&*()])(?=.*\d)[!@#$%^&*()\d]*$/; // 특수문자 + 숫자 조합
	var reg2 = /^(?=.*[A-Za-z])(?=.*[!@#$%^&*()])[A-Za-z!@#$%^&*()]*$/; // 문자 + 특수문자 조합
	var reg3 = /^(?=.*[a-zA-Z])(?=.*\d)(?=.*[!@#$%^&*()])[A-Za-z\d!@#$%^&*()]*$/; // 문자 + 특수문자 + 숫자 조합
	
	if(reg.test(character)||reg1.test(character)||reg2.test(character)||reg3.test(character)){
		return false;
	}else{
		return true;
	}
    
}


// ID 형식 체크 (특수문자는 !#_-만 허용)
function cd_ValidateId(charcter){
	
	var regExp = /^[a-zA-Z0-9]([a-zA-Z0-9!#_-])*$/i ;
	
	return regExp.test(charcter);
	
}


function cf_getSysRegDt() {
	
	var to = new Date();
	var yyyy = to.getFullYear();
	var mm = cf_appendZero(to.getMonth()+1, 2);
	var dd = cf_appendZero(to.getDate(), 2);
	var dtime = yyyy + '-' + mm + '-' + dd;
	
	return dtime;
}


function  cf_qryToObject(querystring) {
		
	var params = {}, pair, d = decodeURIComponent;
	
	if(querystring.indexOf('?') < 1) {
		return false;
	}
	
	querystring = querystring.substring(querystring.indexOf('?')+1).split('&');
 
	for (var i = querystring.length - 1; i >= 0; i--) {
		pair = querystring[i].split('=');
		params[d(pair[0])] = d(pair[1]);
	}
	return params;
};


function cf_jsonToQry(json) {
	
    return '?' +  Object.keys(json).map(function(key) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(json[key]); 
    }).join('&');
    
}


function cf_nvl(val) {
	
	var rtn = '';
	
	if(typeof val !== 'undefined' && val != '') {
		return val;
	}
	
	return rtn;
}